import { accentColor2 } from '../constants/styleVariables';

const textInputTheme = {
    colors: {
        primary: accentColor2,
        placeholder: 'gray',
        background: 'white'
    }
};

const datepickerActive = {
    colors: {
        primary: accentColor2,
        disabled: accentColor2,
        placeholder: accentColor2,
        background: 'white'
    }
};

const grayTextInputTheme = {
    colors: {
        text: 'gray',
        disabled: 'gray',
        primary: 'gray',
        placeholder: 'gray',
        background: 'white'
    }
};

export {
    textInputTheme,
    datepickerActive,
    grayTextInputTheme
};

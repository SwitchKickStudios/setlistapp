const sortDateAsc = (a, b) => dateCompare(a, b, true);
const sortDateDesc = (a, b) => dateCompare(a, b, false);
const sortNameAsc = (a, b) => nameCompare(a, b, true);
const sortNameDesc = (a, b) => nameCompare(a, b, false);

const dateCompare = (a, b, asc) => {
    const dateA = a.date;
    const dateB = b.date;

    let comparison = 0;
    if (dateA > dateB) {
        comparison = 1;
    } else if (dateA < dateB) {
        comparison = -1;
    }
    if (asc) { return comparison; }
    return comparison * -1;
};

const nameCompare = (a, b, asc) => {
    const nameA = a.name;
    const nameB = b.name;

    let comparison = 0;
    if (nameA > nameB) {
        comparison = 1;
    } else if (nameA < nameB) {
        comparison = -1;
    }
    if (asc) { return comparison; }
    return comparison * -1;
};

export {
    sortDateAsc,
    sortDateDesc,
    sortNameAsc,
    sortNameDesc
};

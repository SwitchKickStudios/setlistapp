import * as firebase from 'firebase';

export default async function () {
    const sessionToken = await firebase.auth().currentUser.getIdToken();
    const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${sessionToken}`,
    };
    return headers;
}

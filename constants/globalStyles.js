import { StyleSheet } from 'react-native';
import * as styleVariables from './styleVariables';

export default StyleSheet.create({
    headerButtonText: {
        fontSize: 16,
        color: styleVariables.secondaryColor,
        paddingRight: 12
    },
    actionButtonIconBackground: {
        borderRadius: 20,
        width: 40,
        height: 40,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    centeredFormContainer: {
        flex: 1,
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    fullscreenFlatList: {
        paddingBottom: 80,
        width: '100%'
    },
    columnForm: {
        width: '80%',
        flexDirection: 'column',
        justifyContent: 'center',
    },
    floatingActionButton: {
        backgroundColor: styleVariables.secondaryColor
    },
    bottomDeleteButton: {
        flex: 0,
        width: '80%',
        alignSelf: 'center',
        marginBottom: 12
    },
    songText: {
        fontSize: 18,
    },
    artistText: {
        fontSize: 12,
        paddingTop: 6,
        paddingLeft: 4,
        color: styleVariables.lightGunMetal
    },
    musicalKeyText: {
        fontWeight: '600',
        color: styleVariables.lightGunMetal
    },
    grayLabel: {
        paddingLeft: 4,
        fontWeight: 'normal',
        color: 'gray'
    },
    extendedHeader: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginTop: 8,
        paddingBottom: 12,
        paddingLeft: 12,
    },
    extendedHeaderTitle: {
        color: styleVariables.secondaryColor,
        fontSize: 16
    },
    extendedHeaderToolbar: {
        position: 'absolute',
        right: 12,
        flexDirection: 'row',
        alignItems: 'flex-end'
    },
    closeButtonText: {
        color: 'red'
    },
    miniMenuHeader: {
        paddingBottom: 8,
        paddingLeft: 8,
    },
    miniMenuOpenedAnchor: {
        color: styleVariables.ultralightGunmetal
    },
    complexCellMarginForIcon: {
        marginRight: 46
    },
    complexCellColumn: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'flex-start',
        paddingTop: 16,
        paddingBottom: 16,
        paddingLeft: 16,
        paddingRight: 16,
    },
    complexCellDateView: {
        marginTop: 8,
        marginRight: 20,
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        width: '100%'
    },
});

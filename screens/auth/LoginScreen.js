import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Alert,
} from 'react-native';
import {
    Container,
    Content,
    Button,
    Icon,
    Form
} from 'native-base';
import { CheckBox } from 'react-native-elements';
import * as firebase from 'firebase';
import { connect } from 'react-redux';
import { setIsLoading, setRememberUsername, setStoredUsername } from '../../redux/app-redux';
import WideButton from '../../components/wideButton';
import FormTextInput from '../../components/formTextInput';
import AppLogo from '../../components/appLogo';
import globalStyles from '../../constants/globalStyles';
import { primaryColor, secondaryColor, ultralightGreen } from '../../constants/styleVariables';

class LoginScreen extends React.Component {
    static navigationOptions = () => ({
        headerRight: () => (
            <AppLogo rightPadding />
        ),
    });

    constructor(props) {
        super(props);

        let email = '';
        if (this.getRememberUsername()) {
            email = this.getStoredUsername();
        }

        this.state = {
            email,
            password: '',
        };
    }

    getRememberUsername() {
        const { rememberUsername } = this.props;
        return rememberUsername;
    }

    getStoredUsername() {
        const { storedUsername } = this.props;
        return storedUsername;
    }

    onLoginPress = () => {
        this.props.setIsLoading(true);
        firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
            .then(() => {
                if (this.getRememberUsername()) {
                    this.props.setStoredUsername(this.state.email);
                }
                this.props.setIsLoading(false);
            }, (error) => {
                this.props.setIsLoading(false);
                Alert.alert(error.message);
            });
    }

    onRememberUerPress = () => {
        const newRememberUsername = !this.getRememberUsername();
        this.props.setRememberUsername(newRememberUsername);
    }

    onCreateAccountPress = () => {
        this.props.navigation.navigate('SignUp');
    }

    onForgotPasswordPress = () => {
        this.props.navigation.navigate('ForgotPassword');
    }

    render() {
        return (
            <Container style={styles.container}>
                <Content contentContainerStyle={{ flexGrow: 1 }} style={{ flexGrow: 1 }} scrollEnabled={false}>
                    <View style={globalStyles.centeredFormContainer}>
                        <Form style={styles.form}>
                            <Text style={styles.loginLabel}>Login</Text>

                            {/* EMAIL INPUT */}
                            <FormTextInput
                                label="Email"
                                value={this.state.email}
                                onChangeText={(text) => { this.setState({ email: text }); }}
                                keyboardType="email-address"
                            />

                            {/* PASSWORD INPUT */}
                            <FormTextInput
                                label="Password"
                                value={this.state.password}
                                onChangeText={(text) => { this.setState({ password: text }); }}
                                secureTextEntry
                            />

                            <CheckBox
                                title="Remember User"
                                checkedIcon={<Icon name="check-box" type="MaterialIcons" style={styles.renamedButtonIcon} />}
                                uncheckedIcon={<Icon name="check-box-outline-blank" type="MaterialIcons" style={styles.renameButtonText} />}
                                checked={this.getRememberUsername()}
                                onPress={this.onRememberUerPress}
                                containerStyle={[
                                    styles.renameButtonContainer, (this.getRememberUsername()) ? { backgroundColor: ultralightGreen } : null
                                ]}
                                textStyle={styles.renameButtonText}
                            />

                            {/* LOGIN BUTTON */}
                            <WideButton onPress={this.onLoginPress} text="Log In" />

                            {/* FORGOT PASSWORD BUTTON */}
                            <WideButton onPress={this.onForgotPasswordPress} text="Forgot Password?" color={secondaryColor} />
                        </Form>
                    </View>

                    {/* CREATE ACCOUNT BUTTON */}
                    <View style={styles.footer}>
                        <Button onPress={this.onCreateAccountPress} style={styles.footerButton}>
                            <Text style={styles.createAccountButtonText}>Create Account</Text>
                        </Button>
                    </View>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'white'
    },
    footer: {
        flexDirection: 'row',
        justifyContent: 'center',
        paddingBottom: 30,
        paddingTop: 70
    },
    form: {
        width: '80%',
        flexDirection: 'column',
        justifyContent: 'center',
    },
    loginLabel: {
        color: secondaryColor,
        fontSize: 28,
        paddingLeft: 4
    },
    footerButton: {
        width: 180,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: secondaryColor
    },
    createAccountButtonText: {
        color: 'white',
        fontSize: 18
    },
    renameButtonContainer: {
        width: '100%',
        marginLeft: 1,
        marginRight: 1,
        marginTop: 12,
        marginBottom: 0
    },
    renamedButtonIcon: {
        color: secondaryColor
    },
    renameButtonText: {
        color: primaryColor
    }
});

const mapStateToProps = state => ({
    isLoading: state.isLoading,
    rememberUsername: state.rememberUsername,
    storedUsername: state.storedUsername
});
export default connect(mapStateToProps, { setIsLoading, setRememberUsername, setStoredUsername })(LoginScreen);

import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Alert
} from 'react-native';
import {
    Container,
    Content,
    Form
} from 'native-base';
import * as firebase from 'firebase';
import { connect } from 'react-redux';
import { setIsLoading } from '../../redux/app-redux';
import WideButton from '../../components/wideButton';
import FormTextInput from '../../components/formTextInput';
import AppLogo from '../../components/appLogo';
import globalStyles from '../../constants/globalStyles';
import { secondaryColor } from '../../constants/styleVariables';

class ForgotPasswordScreen extends React.Component {
    static navigationOptions = () => ({
        headerRight: () => (
            <AppLogo rightPadding />
        ),
    });

    constructor(props) {
        super(props);
        this.state = {
            email: '',
        };
    }

    onResetPasswordPress = () => {
        this.props.setIsLoading(true);
        firebase.auth().sendPasswordResetEmail(this.state.email)
            .then(() => {
                this.props.setIsLoading(false);
                Alert.alert(
                    'Password reset email has been sent.',
                    'Please follow the instructions in the email, then log in with your new password.'
                );
            }, (error) => {
                this.props.setIsLoading(false);
                Alert.alert(error.message);
            });
    }

    render() {
        return (
            <Container style={styles.container}>
                <Content contentContainerStyle={{ flexGrow: 1 }} style={{ flexGrow: 1 }} scrollEnabled={false}>
                    <View style={globalStyles.centeredFormContainer}>
                        <Form style={styles.form}>
                            <Text style={styles.forgotPasswordLabel}>Forgot Password?</Text>

                            {/* EMAIL INPUT */}
                            <FormTextInput
                                label="Email"
                                value={this.state.email}
                                onChangeText={(text) => { this.setState({ email: text }); }}
                                keyboardType="email-address"
                            />

                            {/* LOGIN BUTTON */}
                            <WideButton onPress={this.onResetPasswordPress} text="Reset Password" />
                        </Form>
                    </View>

                    <View style={{ height: '25%' }} />
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'white'
    },
    footer: {
        flexDirection: 'row',
        justifyContent: 'center',
        paddingBottom: 30,
        paddingTop: 70
    },
    form: {
        width: '80%',
        flexDirection: 'column',
        justifyContent: 'center',
    },
    forgotPasswordLabel: {
        color: secondaryColor,
        fontSize: 28,
        paddingLeft: 4
    },
});

const mapStateToProps = state => ({ isLoading: state.isLoading });
export default connect(mapStateToProps, { setIsLoading })(ForgotPasswordScreen);

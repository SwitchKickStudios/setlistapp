import React, { Component } from 'react';
import { Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Container, Content, Footer } from 'native-base';
import * as firebase from 'firebase';
import LoadingIndicator from '../components/loadingIndicator';

class UserProfile extends Component {
    static navigationOptions = {
        title: 'User Profile'
    };

    constructor(props) {
        super(props);

        const user = firebase.auth().currentUser;
        this.userRef = firebase.firestore().collection('Users').doc(`${user.uid}`);

        this.userUnsubscribe = null;

        this.state = {
            user: {
                name: ''
            },
            isLoading: true
        };
    }

    componentDidMount() {
        this.userUnsubscribe = this.userRef.onSnapshot(this.checkUserData);
    }

    componentWillUnmount() {
        this.userUnsubscribe();
    }

    checkUserData = (querySnapshot) => {
        const user = {};
        const userData = querySnapshot.data();
        const { name } = userData;
        user.name = name;
        this.setState({
            user,
            isLoading: false
        });
    }

    onChangeUserNamePress = () => {
        this.props.navigation.navigate('CreateUser');
    }

    render() {
        return (
            <Container style={styles.container}>
                <LoadingIndicator isLoading={this.state.isLoading} />
                <Content>
                    <Text>{this.state.user.name}</Text>
                </Content>
                <Footer style={styles.footer}>
                    <TouchableOpacity
                        onPress={this.onChangeUserNamePress}
                        style={[styles.button, styles.changeNameButton]}
                    >
                        <Text style={styles.buttonText}>Change Name</Text>
                    </TouchableOpacity>
                </Footer>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    footer: {
        flexDirection: 'column',
        justifyContent: 'flex-end'
    },
    changeNameButton: {
        backgroundColor: 'navy'
    },
    button: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        height: 50
    },
    buttonText: {
        color: 'white',
        fontSize: 22
    },
});

export default UserProfile;

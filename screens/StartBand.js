import React, { Component } from 'react';
import { Content, Form } from 'native-base';
import {
    Alert,
    Text,
    StyleSheet,
    View,
} from 'react-native';
import * as firebase from 'firebase';
import { connect } from 'react-redux';
import { setIsLoading } from '../redux/app-redux';
import CustomContainer from '../components/customContainer';
import RightHeaderText from '../components/rightHeaderText';
import FormTextInput from '../components/formTextInput';
import WideButton from '../components/wideButton';
import globalStyles from '../constants/globalStyles';
import BandService from '../services/bandService';
import * as globalConstants from '../constants/globalConstants';
import { secondaryColor } from '../constants/styleVariables';

class StartBand extends Component {
    static navigationOptions = {
        headerRight: () => (
            <RightHeaderText text="Start a Band" />
        ),
    };

    constructor(props) {
        super(props);
        this.state = {
            bandName: '',
        };
    }

    submitBand = async () => {
        let alertMessage = 'Error - Problem Creating Band';
        const bandService = new BandService();

        this.props.setIsLoading(true);
        try {
            const bandName = this.state.bandName;
            const uid = firebase.auth().currentUser.uid;
            const result = await bandService.createNewBand(bandName, uid);
            if (result.success) {
                this.props.setIsLoading(false);
                this.props.navigation.navigate('Bands');
                return;
            }
            alertMessage = result.message;
        } catch {
            alertMessage = 'Error - Problem Creating Band';
        }
        this.props.setIsLoading(false);
        Alert.alert(alertMessage);
    }

    updateBandNameTextInput = (bandName) => {
        this.setState({
            bandName
        });
    }

    render() {
        return (
            <CustomContainer style={styles.container} barStyle="default">
                <Content contentContainerStyle={{ flexGrow: 1 }} style={{ flexGrow: 1 }} scrollEnabled={false}>
                    <View style={globalStyles.centeredFormContainer}>
                        <Form style={styles.form}>
                            {/* LABEL */}
                            <Text style={styles.label}>What&#39;s your band called?</Text>

                            {/* BAND NAME INPUT */}
                            <FormTextInput
                                label="Band name"
                                value={this.state.bandName}
                                maxLength={globalConstants.artistMax}
                                onChangeText={text => this.updateBandNameTextInput(text)}
                                keyboardType="default"
                                autoCapitalize="sentences"
                            />

                            {/* CREATE ACCOUNT BUTTON */}
                            <WideButton text="Create Band" onPress={this.submitBand} color={secondaryColor} />
                        </Form>
                    </View>

                    <View style={{ height: '25%' }} />
                </Content>
            </CustomContainer>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    form: {
        width: '80%',
        flexDirection: 'column',
        justifyContent: 'center',
    },
    label: {
        color: secondaryColor,
        fontSize: 26,
        paddingLeft: 2
    },
});

const mapStateToProps = state => ({ isLoading: state.isLoading });
export default connect(mapStateToProps, { setIsLoading })(StartBand);

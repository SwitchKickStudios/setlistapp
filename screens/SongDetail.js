import React, { Component } from 'react';
import {
    Alert,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from 'react-native';
import { Content } from 'native-base';
import * as firebase from 'firebase';
import { connect } from 'react-redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import FormTextInput from '../components/formTextInput';
import CustomContainer from '../components/customContainer';
import { setIsLoading } from '../redux/app-redux';
import { lightGunMetal, headerBorderColor } from '../constants/styleVariables';
import SimpleItemSeparator from '../components/simpleItemSeparator';
import globalStyles from '../constants/globalStyles';
import * as globalConstants from '../constants/globalConstants';

class SongDetail extends Component {
    static navigationOptions = ({ navigation }) => ({
        title: 'Song Detail',
        headerRight: () => (
            (navigation.getParam('isLoaded')) ? (
                <TouchableOpacity onPress={navigation.getParam('toggleEditMode')}>
                    <Text style={globalStyles.headerButtonText}>{navigation.getParam('editMode') ? 'Done' : 'Edit'}</Text>
                </TouchableOpacity>
            ) : null
        ),
    });

    constructor(props) {
        super(props);

        const bandID = this.props.navigation.getParam('bandID', 'Error - Undefined Band ID');
        const setID = this.props.navigation.getParam('setID', 'Error - Undefined Set ID');
        const songID = this.props.navigation.getParam('songID', 'Error - Undefined SongID');

        this.ref = firebase.firestore()
            .collection('Bands').doc(bandID)
            .collection('Sets').doc(setID)
            .collection('Songs').doc(songID);

        this.state = {
            editMode: false,
            songName: '',
            artist: '',
            notes: '',
            musicalKey: '',
            tempo: '',
        };
    }

    componentDidMount() {
        this.props.setIsLoading(true);
        this.unsubscribe = this.ref.onSnapshot(this.onSongUpdate);
        this.props.navigation.setParams({
            toggleEditMode: this.toggleEditMode,
            isLoaded: true
        });
    }

    componentWillUnmount() {
        this.unsubscribe();
    }

    onSongUpdate = (querySnapshot) => {
        const song = querySnapshot.data();
        const {
            songName,
            artist,
            notes,
            tempo,
            musicalKey
        } = song;
        this.setState({
            songName,
            artist,
            notes,
            musicalKey,
            tempo,
        });
        this.props.setIsLoading(false);
    }

    toggleEditMode = () => {
        this.setState((prevState) => {
            const editMode = !prevState.editMode;
            this.props.navigation.setParams({ editMode });
            return { editMode };
        });
    }

    updateSongName = (songName) => {
        this.setState({ songName });
        this.ref.update({ songName });
    }

    updateArtist = (artist) => {
        this.setState({ artist });
        this.ref.update({ artist });
    }

    updateMusicalKey = (musicalKey) => {
        this.setState({ musicalKey });
        this.ref.update({ musicalKey });
    }

    updateTempo = (tempo) => {
        this.setState({ tempo });
        this.ref.update({ tempo });
    }

    updateNotes = (notes) => {
        if (notes.length > globalConstants.songNotesMax) {
            Alert.alert(`Song Notes and Lyrics cannot exceed ${globalConstants.songNotesMax} characters`);
        }
        this.setState({ notes });
        this.ref.update({ notes });
    }

    renderFormInputs() {
        return (
            <KeyboardAwareScrollView>
                <View>
                    {/* Song and Artist */}
                    <View style={styles.songAndArtistView}>
                        <FormTextInput
                            autoCorrect={false}
                            mode="flat"
                            multiline
                            label="Song:"
                            maxLength={globalConstants.songNameMax}
                            value={this.state.songName}
                            onChangeText={text => this.updateSongName(text)}
                            style={styles.headerInput}
                        />
                        <View style={styles.artistInputView}>
                            <FormTextInput
                                autoCorrect={false}
                                mode="flat"
                                label="Artist:"
                                maxLength={globalConstants.artistMax}
                                value={this.state.artist}
                                onChangeText={text => this.updateArtist(text)}
                                style={styles.headerInput}
                            />
                        </View>
                    </View>

                    {/* Key and Tempo */}
                    <View style={styles.keyAndTempoView}>
                        <View style={styles.keyInputView}>
                            <FormTextInput
                                autoCorrect={false}
                                mode="flat"
                                label="Key:"
                                maxLength={globalConstants.musicalKeyMax}
                                value={this.state.musicalKey}
                                onChangeText={text => this.updateMusicalKey(text)}
                                style={styles.headerInput}
                            />
                        </View>
                        <View style={styles.editTempoView}>
                            <FormTextInput
                                autoCorrect={false}
                                mode="flat"
                                label="Tempo:"
                                maxLength={globalConstants.tempoMax}
                                value={this.state.tempo}
                                onChangeText={text => this.updateTempo(text)}
                                style={[styles.headerInput, styles.tempoInput]}
                            />
                            <Text style={[globalStyles.grayLabel, styles.bpmLabel, styles.editBpmLabel]}>bpm</Text>
                        </View>
                    </View>
                </View>
                <SimpleItemSeparator style={{ backgroundColor: headerBorderColor, marginTop: 8 }} />
                {/* Song Notes */}
                <TextInput
                    multiline
                    style={styles.notesTextArea}
                    autoCorrect={false}
                    onChangeText={text => this.updateNotes(text)}
                    placeholder="Enter notes and lyrics here..."
                    value={this.state.notes}
                    maxLength={globalConstants.songNotesMax}
                    scrollEnabled={false}
                />
            </KeyboardAwareScrollView>
        );
    }

    renderDisplay() {
        return (
            <Content contentContainerStyle={{ flexGrow: 1 }} style={{ flexGrow: 1 }}>
                <View style={styles.songAndArtistDisplayView}>
                    <Text style={styles.songText}>{this.state.songName}</Text>
                    <Text style={styles.artistText}>{this.state.artist}</Text>
                </View>
                <View style={styles.keyAndTempoDisplayView}>
                    <View style={[styles.displayRowView, styles.musicalKeyView]}>
                        <Text style={globalStyles.musicalKeyText}>{this.state.musicalKey}</Text>
                    </View>
                    <View style={styles.displayRowView}>
                        <Text style={globalStyles.musicalKeyText}>{this.state.tempo}</Text>
                        <Text style={[globalStyles.grayLabel, styles.bpmLabel]}>{(this.state.tempo) ? 'bpm' : null}</Text>
                    </View>
                </View>
                <SimpleItemSeparator style={{ backgroundColor: headerBorderColor }} />
                <View style={styles.notesView}>
                    <Text>{this.state.notes}</Text>
                </View>
            </Content>
        );
    }

    // Determine if we should show the screen as a form if in edit mode or in a normal display
    renderContent() {
        if (this.state.editMode) {
            return this.renderFormInputs();
        }
        return this.renderDisplay();
    }

    render() {
        return (
            <CustomContainer style={styles.container} barStyle="default">
                {this.renderContent()}
            </CustomContainer>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    // EDIT MODE
    songAndArtistView: {
        paddingTop: 8,
        paddingHorizontal: 8
    },
    artistInputView: {
        paddingTop: 8
    },
    keyAndTempoView: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        paddingVertical: 8,
        paddingHorizontal: 8
    },
    headerInput: {
        marginTop: 0
    },
    keyInputView: {
        width: '65%'
    },
    editTempoView: {
        flexDirection: 'row',
        justifyContent: 'center',
        width: '35%'
    },
    tempoInput: {
        flexGrow: 1,
        marginLeft: 8,
    },
    notesTextArea: {
        minHeight: 200,
        paddingHorizontal: 8
    },
    editBpmLabel: {
        position: 'absolute',
        right: 8,
        bottom: 8
    },
    // DISPLAY MODE
    songAndArtistDisplayView: {
        padding: 12
    },
    keyAndTempoDisplayView: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingVertical: 12,
        paddingHorizontal: 12,
    },
    songText: {
        fontSize: 24,
    },
    artistText: {
        fontSize: 18,
        paddingTop: 6,
        paddingLeft: 4,
        color: lightGunMetal
    },
    displayRowView: {
        flexGrow: 1,
        maxWidth: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    bpmLabel: {
        fontSize: 12,
        paddingTop: 2
    },
    musicalKeyView: {
        maxWidth: '75%'
    },
    notesView: {
        padding: 12
    }
});

const mapStateToProps = state => ({ isLoading: state.isLoading });
export default connect(mapStateToProps, { setIsLoading })(SongDetail);

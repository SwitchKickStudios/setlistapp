import React, { Component } from 'react';
import { Content, Form } from 'native-base';
import {
    Alert,
    StyleSheet,
    View,
    Text
} from 'react-native';
import { connect } from 'react-redux';
import { setIsLoading } from '../redux/app-redux';
import CustomAlert from '../components/customAlert';
import RightHeaderText from '../components/rightHeaderText';
import FormTextInput from '../components/formTextInput';
import WideButton from '../components/wideButton';
import CustomContainer from '../components/customContainer';
import globalStyles from '../constants/globalStyles';
import { secondaryColor } from '../constants/styleVariables';
import * as globalConstants from '../constants/globalConstants';
import BandMemberService from '../services/bandMemberService';

class InviteBandMember extends Component {
    static navigationOptions = {
        headerRight: () => (
            <RightHeaderText text="Invite Band Member" />
        ),
    };

    constructor(props) {
        super(props);

        const bandID = this.props.navigation.getParam('bandID', 'Error - Undefined Band ID');
        this.state = {
            bandID,
            phone: '',
        };
    }

    onSubmit = async () => {
        if (this.state.phone.length < 1) {
            Alert.alert('Invalid Phone Number', 'Phone number can\'t be empty.');
            return;
        }

        const bandMemberService = new BandMemberService();
        this.props.setIsLoading(true);
        try {
            const result = await bandMemberService.inviteBandMember(this.state.phone, this.state.bandID);
            this.props.setIsLoading(false);
            CustomAlert.simpleWithCompletion(result.message, () => {
                this.props.navigation.goBack();
            });
        } catch {
            this.props.setIsLoading(false);
            Alert.alert('Error', 'Problem Creating New Band Invitation.');
        }
    }

    render() {
        return (
            <CustomContainer style={styles.container} bounces={false} barStyle="default">
                <Content contentContainerStyle={{ flexGrow: 1 }} style={{ flexGrow: 1 }} scrollEnabled={false}>
                    <View style={globalStyles.centeredFormContainer}>
                        <Form style={globalStyles.columnForm}>
                            {/* LABEL */}
                            <Text style={styles.label}>Enter Phone Number:</Text>

                            {/* SET Name INPUT */}
                            <FormTextInput
                                label="Phone Number"
                                value={this.state.setName}
                                onChangeText={text => this.setState({ phone: text })}
                                keyboardType="phone-pad"
                                maxLength={globalConstants.phoneNumberMax}
                                autoCorrect={false}
                            />
                            {/* CREATE SET BUTTON */}
                            <WideButton text="Send" onPress={this.onSubmit} color={secondaryColor} />
                        </Form>
                    </View>

                    <View style={{ height: '25%' }} />
                </Content>
            </CustomContainer>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    label: {
        color: secondaryColor,
        fontSize: 28,
        paddingLeft: 3
    },
});

const mapStateToProps = state => ({ isLoading: state.isLoading });

export default connect(mapStateToProps, { setIsLoading })(InviteBandMember);

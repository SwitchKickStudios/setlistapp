import React from 'react';
import {
    Alert,
    StyleSheet,
    Text,
    View,
} from 'react-native';
import { Content, Form } from 'native-base';
import { connect } from 'react-redux';
import { StackActions, NavigationActions } from 'react-navigation';
import { setIsLoading } from '../redux/app-redux';
import BandMemberService from '../services/bandMemberService';
import CustomContainer from '../components/customContainer';
import RightHeaderText from '../components/rightHeaderText';
import FormTextInput from '../components/formTextInput';
import WideButton from '../components/wideButton';
import globalStyles from '../constants/globalStyles';
import * as globalConstants from '../constants/globalConstants';
import { secondaryColor } from '../constants/styleVariables';

class CreateBandMember extends React.Component {
    static navigationOptions = () => ({
        headerRight: () => (
            <RightHeaderText text="Display Name" />
        ),
    });

    constructor(props) {
        super(props);
        this.state = {
            displayName: '',
        };
    }

    onSubmit = async () => {
        if (this.state.displayName.length < 1) {
            Alert.alert('Invalid Display Name', 'Display name can\'t be empty.');
            return;
        }

        let alertMessage = 'Error - Problem Creating New User';
        const bandMemberService = new BandMemberService();
        this.props.setIsLoading(true);
        try {
            const result = await bandMemberService.createUser(this.state.displayName);
            if (result.success) {
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'Bands' })],
                });
                this.props.navigation.dispatch(resetAction);
                return;
            }
            alertMessage = result.message;
        } catch {
            alertMessage = 'Error - Problem Creating New User';
        }
        this.props.setIsLoading(false);
        Alert.alert(alertMessage);
    }

    render() {
        return (
            <CustomContainer style={styles.container} barStyle="default">
                <Content contentContainerStyle={{ flexGrow: 1 }} style={{ flexGrow: 1 }} scrollEnabled={false}>
                    <View style={globalStyles.centeredFormContainer}>
                        <Form style={styles.form}>
                            {/* LABEL */}
                            <Text style={styles.label}>What&#39;s your name?</Text>

                            {/* DISPLAY NAME INPUT */}
                            <FormTextInput
                                label="Enter your display name"
                                value={this.state.displayName}
                                maxLength={globalConstants.displayNameMax}
                                onChangeText={(text) => { this.setState({ displayName: text }); }}
                                keyboardType="default"
                                autoCapitalize="sentences"
                            />
                            {/* CREATE ACCOUNT BUTTON */}
                            <WideButton text="Save" onPress={this.onSubmit} color={secondaryColor} />
                        </Form>
                    </View>

                    <View style={{ height: '25%' }} />
                </Content>
            </CustomContainer>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    form: {
        width: '80%',
        flexDirection: 'column',
        justifyContent: 'center',
    },
    label: {
        color: secondaryColor,
        fontSize: 28,
        paddingLeft: 4
    },
});

const mapStateToProps = state => ({ isLoading: state.isLoading });

export default connect(mapStateToProps, { setIsLoading })(CreateBandMember);

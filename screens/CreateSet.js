import React, { Component } from 'react';
import { Content, Form } from 'native-base';
import {
    Alert,
    Keyboard,
    Text,
    StyleSheet,
    View
} from 'react-native';
import Moment from 'moment';
import { connect } from 'react-redux';
import { setIsLoading } from '../redux/app-redux';
import CustomContainer from '../components/customContainer';
import RightHeaderText from '../components/rightHeaderText';
import FormTextInput from '../components/formTextInput';
import WideButton from '../components/wideButton';
import DatePicker from '../components/datePicker';
import SetsService from '../services/setsService';
import globalStyles from '../constants/globalStyles';
import { secondaryColor } from '../constants/styleVariables';
import * as globalConstants from '../constants/globalConstants';

class CreateSet extends Component {
    static navigationOptions = {
        headerRight: () => (
            <RightHeaderText text="New Setlist" />
        ),
    };

    constructor(props) {
        super(props);
        Moment.locale('en');
        const currentDate = new Date();
        const formattedDate = Moment(currentDate).format('MMMM D, YYYY');

        const bandID = this.props.navigation.getParam('bandID', 'Error - Undefined Band ID');
        this.state = {
            bandID,
            setName: '',
            setDate: currentDate,
            setDateDisplay: formattedDate
        };
    }

    submitSet = async () => {
        const { setName, setDate, bandID } = this.state;
        if (setName.length < 1) {
            Alert.alert('Invalid Set Name', 'Set name can\'t be empty.');
            return;
        }
        if (setDate.toString().length < 1) {
            Alert.alert('Invalid Set Date', 'Date can\'t be empty.');
            return;
        }

        let alertMessage = 'Error - Problem Creating Set';
        const setsService = new SetsService();
        this.props.setIsLoading(true);
        try {
            const result = await setsService.createSet(setName, setDate, bandID);
            if (result.success) {
                this.props.navigation.navigate('Sets', { bandID: this.state.bandID });
                return;
            }
            alertMessage = result.message;
        } catch {
            alertMessage = 'Error - Problem Creating Set';
        }
        this.props.setIsLoading(false);
        Alert.alert(alertMessage);
    }

    updateSetNameTextInput = (setName) => {
        this.setState({ setName });
    }

    onConfirm = (date) => {
        Moment.locale('en');
        const formattedDate = Moment(date).format('MMMM D, YYYY');
        this.setState({ setDate: date, setDateDisplay: `${formattedDate}` });
    }

    onOpenDatePicker = () => {
        Keyboard.dismiss();
    }

    render() {
        return (
            <CustomContainer style={styles.container} barStyle="default">
                <Content contentContainerStyle={{ flexGrow: 1 }} style={{ flexGrow: 1 }} scrollEnabled={false}>
                    <View style={globalStyles.centeredFormContainer}>
                        <Form style={globalStyles.columnForm}>
                            {/* LABEL */}
                            <Text style={styles.label}>Create set:</Text>

                            {/* SET Name INPUT */}
                            <FormTextInput
                                label="Enter your set's name"
                                value={this.state.setName}
                                onChangeText={text => this.updateSetNameTextInput(text)}
                                keyboardType="default"
                                autoCapitalize="sentences"
                                multiline
                                maxLength={globalConstants.setMax}
                            />

                            <DatePicker
                                onConfirm={this.onConfirm}
                                onOpenDatePicker={this.onOpenDatePicker}
                                value={this.state.setDateDisplay}
                                date={this.state.setDate}
                            />
                            {/* CREATE SET BUTTON */}
                            <WideButton text="Create Set" onPress={this.submitSet} color={secondaryColor} />
                        </Form>
                    </View>

                    <View style={{ height: '25%' }} />
                </Content>
            </CustomContainer>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    label: {
        color: secondaryColor,
        fontSize: 28,
        paddingLeft: 3
    },
    dateLabel: {
        marginTop: 16
    }
});

const mapStateToProps = state => ({ isLoading: state.isLoading });

export default connect(mapStateToProps, { setIsLoading })(CreateSet);

import React, { Component } from 'react';
import {
    Text,
    FlatList,
    TouchableOpacity,
    Alert,
    View
} from 'react-native';
import Moment from 'moment';
import { Icon } from 'native-base';
import * as firebase from 'firebase';
import { connect } from 'react-redux';
import {
    setIsLoading,
    setSongsShowDate,
    setSongsSortMode
} from '../redux/app-redux';
import SongsService from '../services/songsService';
import {
    sortDateAsc,
    sortDateDesc,
    sortNameAsc,
    sortNameDesc
} from '../utilities/sortFunctions';
import EmptyContentMessage from '../components/emptyContentMessage';
import WideButton from '../components/wideButton';
import SongTableItem from '../components/songTableItem';
import globalStyles from '../constants/globalStyles';
import EditButton from '../components/editButton';
import ProfileDrawerButton from '../components/profileDrawerButton';
import ExtendedHeader from '../components/extendedHeader';
import CustomContainer from '../components/customContainer';
import NavigationAwareFAB from '../components/navigationAwareFAB';
import ShowHideButton from '../components/showHideButton';
import SortButton from '../components/sortButton';

class Songs extends Component {
    static navigationOptions = ({ navigation }) => {
        const { params } = navigation.state;
        return {
            title: params ? params.setName : 'Songs',
            headerRight: () => (
                params && params.songsDeleteMode ? (
                    <TouchableOpacity onPress={() => params.doneButtonAction()}>
                        <Text style={[globalStyles.headerButtonText, globalStyles.closeButtonText]}>Close</Text>
                    </TouchableOpacity>
                ) : (
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <EditButton onPress={() => params.editButtonAction()} />
                        <ProfileDrawerButton onPress={() => navigation.openDrawer()} />
                    </View>
                )
            )
        };
    }

    constructor(props) {
        super(props);

        const bandID = this.props.navigation.getParam('bandID', 'Error - Undefined Band ID');
        const setID = this.props.navigation.getParam('setID', 'Error - Undefined Set ID');
        const setName = this.props.navigation.getParam('setName', 'Error - Undefined Set Date');
        const setDate = this.props.navigation.getParam('date', 'Error - Undefined Set Date');

        this.ref = firebase.firestore()
            .collection('Bands').doc(bandID)
            .collection('Sets').doc(setID)
            .collection('Songs');
        this.setRef = firebase.firestore()
            .collection('Bands').doc(bandID)
            .collection('Sets').doc(setID);

        this.unsubscribe = null;
        this.setUnsubstribe = null;

        this.state = {
            songs: [],
            deleteMode: false,
            bandID,
            setID,
            setName,
            setDate,
        };
    }

    componentDidMount() {
        this.props.setIsLoading(true);
        this.props.navigation.setParams({ doneButtonAction: this.doneButtonAction });
        this.props.navigation.setParams({ editButtonAction: this.editButtonAction });
        this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
        this.setUnsubstribe = this.setRef.onSnapshot(this.onSetUpdate);
    }

    componentWillUnmount() {
        this.unsubscribe();
    }

    onCollectionUpdate = (querySnapshot) => {
        const songs = [];
        querySnapshot.forEach((doc) => {
            const {
                songName,
                artist,
                musicalKey,
                tempo,
                date
            } = doc.data();
            songs.push({
                key: doc.id, // Document ID
                songName,
                artist,
                tempo,
                musicalKey,
                date,
                isSelected: false
            });
        });

        this.setState({ songs });
        this.props.setIsLoading(false);
    }

    onSetUpdate = (querySnapshot) => {
        if (querySnapshot.exists) {
            const { name, date } = querySnapshot.data();
            this.setState({ setName: name, setDate: date });
            this.props.navigation.setParams({ setName: name });
        }
    }

    getSongsShowDate() {
        const { songsShowDate } = this.props;
        return songsShowDate;
    }

    getSongsSortMode() {
        const { songsSortMode } = this.props;
        return songsSortMode;
    }

    onShowDatePress = () => {
        const nextDateShowHide = !this.getSongsShowDate();
        this.props.setSongsShowDate(nextDateShowHide);
    }

    onNameAscPress = () => {
        this.props.setSongsSortMode('nameAsc');
    }

    onNameDescPress = () => {
        this.props.setSongsSortMode('nameDesc');
    }

    onDateAscPress = () => {
        this.props.setSongsSortMode('dateAsc');
    }

    onDateDescPress = () => {
        this.props.setSongsSortMode('dateDesc');
    }

    // Method for handing the delete buttons on the FlatList items
    onDeleteSong = (bandID, setID, songObject) => {
        const { key } = songObject;
        this.setState((prevState) => {
            const nextState = prevState;
            const songsArray = prevState.songs;

            const selectedSongIndex = songsArray.findIndex((songItem) => {
                if (songItem.key === key) {
                    return true;
                }
                return false;
            });
            const selectedSong = songsArray[selectedSongIndex];
            selectedSong.isSelected = !selectedSong.isSelected;
            nextState.songs[selectedSongIndex] = selectedSong;
            return nextState;
        });
    }

    getSetDate() {
        const date = this.state.setDate;
        const formattedDate = Moment(date).format('MMMM D, YYYY');
        return formattedDate;
    }

    // Action after accepting alert to delete selected songs
    deleteBatchSongs = async (selectedSongs) => {
        const { bandID, setID } = this.state;
        const songsService = new SongsService();
        let alertMessage = 'Error - Problem Deleting Songs';
        const songIDs = [];

        // Load up array of songIDs for selected songs
        selectedSongs.forEach((song) => {
            songIDs.push(song.key);
        });

        this.props.setIsLoading(true);
        try {
            // Call deleteBatchSongs via SongsService
            const result = await songsService.deleteBatchSongs(bandID, setID, songIDs);
            alertMessage = result.message;
        } catch {
            alertMessage = 'Error - Problem Deleting Song';
        }
        this.props.setIsLoading(false);
        Alert.alert(alertMessage);
    }

    onSongSelect = (bandID, setID, songObject) => {
        const { key } = songObject;
        this.props.navigation.navigate('SongDetail', {
            bandID,
            setID,
            songID: key,
            isLoaded: false
        });
    }

    // Header button action for activating edit mode
    editButtonAction = () => {
        const {
            bandID,
            setID,
            setName,
            setDate
        } = this.state;
        this.props.navigation.navigate('EditSet', {
            bandID,
            setID,
            setName,
            setDate
        });
    }

    // Header button action for turning off deleteMode
    doneButtonAction = () => {
        this.setState({ deleteMode: false });
        this.props.navigation.setParams({ songsDeleteMode: false });
    }

    // Method for the Create Song action button - navigate to create song
    onCreateSongPress = () => {
        this.props.navigation.navigate('CreateSong', {
            bandID: this.state.bandID,
            setID: this.state.setID,
            setName: this.props.navigation.getParam('setName', 'Error - Undefined Set Name')
        });
    }

    // Method for the Delete Set action button - toggle Delete Mode ON
    onDeleteSongPress = () => {
        this.props.navigation.setParams({ songsDeleteMode: true });
        this.setState({ deleteMode: true });
    }

    // Handles action for bottom Delete Songs button
    // Filter songs from local state to find selected Songs
    // Throw up an alert before performing batch delete
    onDeleteButtonPress = () => {
        const { songs } = this.state;
        const selectedSongs = songs.filter((song) => {
            if (song.isSelected) {
                return true;
            }
            return false;
        });
        Alert.alert(
            `Are you sure you want to delete these ${selectedSongs.length} songs?`,
            'All selected song data will be erased. This action is not reversible.',
            [
                { text: 'Cancel', onPress: () => console.log('Cancelled - Song Delete') },
                { text: 'OK', onPress: () => this.deleteBatchSongs(selectedSongs) },
                // { text: 'OK', onPress: () => console.log('Deleting songs...') },
            ],
            { cancelable: false }
        );
    }

    determineDeleteButtonColor() {
        const { songs } = this.state;
        const selectedSong = songs.findIndex((song) => {
            if (song.isSelected) {
                return true;
            }
            return false;
        });
        if (selectedSong >= 0) {
            return 'red';
        }
        return 'gray';
    }

    renderSetList() {
        if (this.state.songs.length > 0) {
            return (
                <FlatList
                    data={this.state.songs}
                    extraData={[this.state, this.getSongsShowDate()]}
                    style={globalStyles.fullscreenFlatList}
                    renderItem={
                        ({ item }) => (
                            <SongTableItem
                                onPress={() => this.onSongSelect(this.state.bandID, this.state.setID, item)}
                                onDelete={() => this.onDeleteSong(this.state.bandID, this.state.setID, item)}
                                isLoading={item.isLoading}
                                songname={item.songName}
                                artist={item.artist}
                                tempo={item.tempo}
                                musicalKey={item.musicalKey}
                                date={item.date}
                                deleteMode={this.state.deleteMode}
                                isSelected={item.isSelected}
                                showDate={this.getSongsShowDate()}
                            />
                        )
                    }
                />
            );
        }
        return <EmptyContentMessage message="This setlist doesn't have any songs yet." />;
    }

    renderActionButton(actions) {
        return (
            <NavigationAwareFAB
                open={this.state.FABOpen}
                onStateChange={({ open }) => this.setState({ FABOpen: open })}
                actions={actions}
                icon={({ size, color }) => (<Icon name="music-note" type="MaterialIcons" style={{ fontSize: size, color }} />)}
            />
        );
    }

    renderBottomButtons() {
        if (this.state.deleteMode) {
            return (
                <WideButton
                    onPress={this.onDeleteButtonPress}
                    text="Delete Songs"
                    buttonStyle={globalStyles.bottomDeleteButton}
                    color={this.determineDeleteButtonColor()}
                    disabled={this.determineDeleteButtonColor() === 'gray'}
                />
            );
        }

        if (this.state.songs.length < 1) {
            const actions = [
                {
                    icon: (({ size, color }) => (
                        <Icon name="file-plus" type="MaterialCommunityIcons" style={{ fontSize: size, color }} />
                    )),
                    label: 'Create Song',
                    onPress: () => this.onCreateSongPress()
                },
            ];
            return this.renderActionButton(actions);
        }
        const actions = [
            {
                icon: (({ size, color }) => (
                    <Icon name="file-plus" type="MaterialCommunityIcons" style={{ fontSize: size, color }} />
                )),
                label: 'Create Song',
                onPress: () => this.onCreateSongPress()
            },
            {
                icon: (({ color }) => (
                    <Icon name="delete-forever" type="MaterialIcons" style={{ fontSize: 25, color }} />
                )),
                label: 'Delete Song',
                onPress: () => this.onDeleteSongPress()
            },
        ];
        return this.renderActionButton(actions);
    }

    render() {
        return (
            <CustomContainer barStyle="default" hideStatusBar={this.props.navigation.state.isDrawerOpen}>
                <ExtendedHeader style={[globalStyles.extendedHeader, { justifyContent: 'flex-end', marginRight: 12 }]} hideBorder>
                    <Text style={globalStyles.extendedHeaderTitle}>Date: {this.getSetDate()}</Text>
                </ExtendedHeader>

                <ExtendedHeader style={globalStyles.extendedHeader}>
                    <Text style={globalStyles.extendedHeaderTitle}>Songs</Text>
                    <View style={[globalStyles.extendedHeaderToolbar]}>
                        <ShowHideButton onShowDatePress={this.onShowDatePress} showDate={this.getSongsShowDate()} />
                        <SortButton
                            onNameAscPress={this.onNameAscPress}
                            onNameDescPress={this.onNameDescPress}
                            onDateAscPress={this.onDateAscPress}
                            onDateDescPress={this.onDateDescPress}
                        />
                    </View>
                </ExtendedHeader>
                {this.renderSetList()}
                {this.renderBottomButtons()}
            </CustomContainer>
        );
    }
}

const mapStateToProps = state => ({
    isLoading: state.isLoading,
    songsShowDate: state.songsShowDate,
    songsSortMode: state.songsSortMode
});
export default connect(mapStateToProps, {
    setIsLoading,
    setSongsShowDate,
    setSongsSortMode
})(Songs);

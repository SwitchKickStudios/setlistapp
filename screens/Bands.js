import React, { Component } from 'react';
import {
    Text,
    TouchableOpacity,
    StyleSheet,
    FlatList,
    Alert,
} from 'react-native';
import { Icon } from 'native-base';
import * as firebase from 'firebase';
import { connect } from 'react-redux';
import {
    setUserDisplayName,
    setBandsDeleteMode,
    setBandsRenameMode,
    setIsLoading,
    setCurrentBandID
} from '../redux/app-redux';
import BandsTableItem from '../components/bandsTableItem';
import LeftHeaderText from '../components/leftHeaderText';
import EmptyContentMessage from '../components/emptyContentMessage';
import WideButton from '../components/wideButton';
import globalStyles from '../constants/globalStyles';
import BandMemberService from '../services/bandMemberService';
import ProfileDrawerButton from '../components/profileDrawerButton';
import ExtendedHeader from '../components/extendedHeader';
import CustomContainer from '../components/customContainer';
import NavigationAwareFAB from '../components/navigationAwareFAB';

class Bands extends Component {
    static navigationOptions = ({ navigation }) => {
        const { params } = navigation.state;
        return {
            headerLeft: () => (
                <LeftHeaderText text={params ? params.screenTitle : ''} />
            ),
            headerRight: () => (
                params && (!params.bandsDeleteMode && !params.bandsRenameMode) ? (
                    <ProfileDrawerButton onPress={() => navigation.openDrawer()} />
                ) : (
                    <TouchableOpacity onPress={() => params.doneButtonAction()}>
                        <Text style={[globalStyles.headerButtonText, globalStyles.closeButtonText]}>Close</Text>
                    </TouchableOpacity>
                )
            )
        };
    }

    constructor(props) {
        super(props);
        const user = firebase.auth().currentUser;
        this.ref = firebase.firestore().collection('Bands').where(`users.${user.uid}.write_permission`, '==', true);
        this.userRef = firebase.firestore().collection('Users').doc(`${user.uid}`);

        this.unsubscribe = null;
        this.userUnsubscribe = null;

        this.state = {
            bands: [],
            selectedForDelete: null,
            FABOpen: false
        };
    }

    componentDidMount() {
        this.props.navigation.setParams({ doneButtonAction: this.doneButtonAction });
        this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
        this.userUnsubscribe = this.userRef.get().then(this.checkUserData);
    }

    componentDidUpdate() {
        if (this.props.navigation.getParam('bandsDeleteMode') !== this.props.bandsDeleteMode) {
            this.props.navigation.setParams({ bandsDeleteMode: this.props.bandsDeleteMode });
        }
        if (this.props.navigation.getParam('bandsRenameMode') !== this.props.bandsRenameMode) {
            this.props.navigation.setParams({ bandsRenameMode: this.props.bandsRenameMode });
        }
    }

    componentWillUnmount() {
        this.unsubscribe();
    }

    onCollectionUpdate = (querySnapshot) => {
        const bands = [];
        querySnapshot.forEach((doc) => {
            const { bandData } = doc.data();
            bands.push({
                key: doc.id, // Document ID
                doc, // DocumentSnapshot
                bandData,
            });
        });
        this.props.setIsLoading(false);
        this.setState({ bands });
    }

    onBandSelect = (bandID) => {
        this.props.setCurrentBandID(bandID);
        this.props.navigation.navigate('Sets', { bandID });
    }

    // Method for handing the delete buttons on the FlatList items
    onSelectForDelete = (bandID) => {
        if (this.state.selectedForDelete === bandID) {
            this.setState({ selectedForDelete: null });
        } else {
            this.setState({ selectedForDelete: bandID });
        }
    }

    onLeaveBand = () => {
        const { bands, selectedForDelete } = this.state;
        let leavingBand = null;

        bands.forEach((band) => {
            if (band.key === selectedForDelete) {
                leavingBand = band;
            }
        });

        Alert.alert(
            `Are you sure you want to leave ${leavingBand.bandData.name}?`,
            "If you currently are the only member of this band, all of this band's data will be erased. This action is not reversible.",
            [
                { text: 'Cancel', onPress: () => console.log('Cancelled - Leaved Band') },
                { text: 'OK', onPress: () => this.leaveBand(leavingBand.key) },
            ],
            { cancelable: false }
        );
    }

    onRenameBand = (bandID, bandName) => {
        this.props.navigation.navigate('RenameBand', { bandID, bandName });
    }

    onStartBandPress = () => {
        this.props.navigation.navigate('StartBand');
    }

    onRenameBandPress = () => {
        this.props.setBandsRenameMode(true);
    }

    onJoinBandPress = () => {
        this.props.navigation.navigate('JoinBand');
    }

    onLeaveBandPress = () => {
        this.props.setBandsDeleteMode(true);
    }

    getBandsDeleteMode() {
        const { bandsDeleteMode } = this.props;
        return bandsDeleteMode;
    }

    getBandsRenameMode() {
        const { bandsRenameMode } = this.props;
        return bandsRenameMode;
    }

    doneButtonAction = () => {
        this.props.setBandsDeleteMode(false);
        this.props.setBandsRenameMode(false);
    }

    checkUserData = (querySnapshot) => {
        if (querySnapshot.exists) {
            const displayName = querySnapshot.data().name;
            this.props.setUserDisplayName(displayName);
            this.props.navigation.setParams({ screenTitle: displayName });
        } else {
            this.props.navigation.navigate('CreateUser');
        }
    }

    // Use BandMemberService to delete user from Band
    leaveBand = async (bandID) => {
        const bandMemberService = new BandMemberService();
        let alertMessage = 'Error - Problem Leavin Band';
        this.props.setIsLoading(true);
        try {
            const result = await bandMemberService.deleteBandMember(bandID);
            alertMessage = result.message;
        } catch {
            alertMessage = 'Error - Problem Leaving Band';
        }
        this.props.setBandsDeleteMode(false);
        this.props.setIsLoading(false);
        this.setState({ selectedForDelete: null });
        Alert.alert(alertMessage);
    }

    determineLeaveBandButtonColor() {
        const { selectedForDelete } = this.state;
        if (selectedForDelete) {
            return 'red';
        }
        return 'gray';
    }

    renderBandList() {
        if (this.state.bands.length > 0) {
            return (
                <FlatList
                    data={this.state.bands}
                    extraData={[this.state, this.getBandsDeleteMode(), this.getBandsRenameMode()]}
                    style={globalStyles.fullscreenFlatList}
                    renderItem={
                        ({ item }) => (
                            <BandsTableItem
                                onPress={() => this.onBandSelect(item.key)}
                                onDelete={() => this.onSelectForDelete(item.key)}
                                onRename={() => this.onRenameBand(item.key, item.bandData.name)}
                                renderContent={<Text>{item.bandData.name}</Text>}
                                deleteMode={this.getBandsDeleteMode()}
                                renameMode={this.getBandsRenameMode()}
                                deleteButtonText="Leave Band"
                                selectedForDelete={(this.state.selectedForDelete === item.key)}
                            />
                        )
                    }
                />
            );
        }
        return <EmptyContentMessage message="You're not a member of a band yet." />;
    }

    renderBottomButtons() {
        if (this.getBandsDeleteMode()) {
            return (
                <WideButton
                    onPress={this.onLeaveBand}
                    text="Leave Band"
                    buttonStyle={globalStyles.bottomDeleteButton}
                    color={this.determineLeaveBandButtonColor()}
                    disabled={this.determineLeaveBandButtonColor() === 'gray'}
                />
            );
        }

        if (this.getBandsRenameMode()) {
            return null;
        }

        const actions = [
            {
                icon: (({ color }) => (
                    <Icon name="guitar-electric" type="MaterialCommunityIcons" style={{ fontSize: 25, color }} />
                )),
                label: 'Start Band',
                onPress: () => this.onStartBandPress()
            },
            {
                icon: (({ size, color }) => (
                    <Icon name="pencil" type="MaterialCommunityIcons" style={{ fontSize: size, color }} />
                )),
                label: 'Rename Band',
                onPress: () => this.onRenameBandPress()
            },
            {
                icon: (({ size, color }) => (
                    <Icon name="group-add" type="MaterialIcons" style={{ fontSize: size, color }} />
                )),
                label: 'Join Band',
                onPress: () => this.onJoinBandPress()
            },
            {
                icon: (({ size, color }) => (
                    <Icon name="account-multiple-minus" type="MaterialCommunityIcons" style={{ fontSize: size, color }} />
                )),
                label: 'Leave Band',
                onPress: () => this.onLeaveBandPress()
            },
        ];

        return (
            <NavigationAwareFAB
                open={this.state.FABOpen}
                onStateChange={({ open }) => this.setState({ FABOpen: open })}
                actions={actions}
                icon="add"
            />
        );
    }

    render() {
        return (
            <CustomContainer style={styles.container} barStyle="default" hideStatusBar={this.props.navigation.state.isDrawerOpen}>
                <ExtendedHeader style={globalStyles.extendedHeader}>
                    <Text style={globalStyles.extendedHeaderTitle}>Bands</Text>
                </ExtendedHeader>
                {this.renderBandList()}
                {this.renderBottomButtons()}
            </CustomContainer>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
});

const mapStateToProps = state => ({
    userDisplayName: state.userDisplayName,
    bandsDeleteMode: state.bandsDeleteMode,
    bandsRenameMode: state.bandsRenameMode,
    isLoading: state.isLoading
});
export default connect(mapStateToProps, {
    setUserDisplayName,
    setBandsDeleteMode,
    setBandsRenameMode,
    setIsLoading,
    setCurrentBandID
})(Bands);

import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    FlatList,
    View
} from 'react-native';
import * as firebase from 'firebase';
import { connect } from 'react-redux';
import { setIsLoading } from '../redux/app-redux';
import CustomContainer from '../components/customContainer';
import RightHeaderText from '../components/rightHeaderText';
import SimpleTableItem from '../components/simpleTableItem';
import SectionHeader from '../components/sectionHeader';
import WideButton from '../components/wideButton';
import { lightGreen } from '../constants/styleVariables';

class BandDetail extends Component {
    static navigationOptions = ({ navigation }) => ({
        headerRight: () => (
            <View>
                <RightHeaderText text={navigation.state.params.bandName} />
            </View>
        )
    });

    constructor(props) {
        super(props);
        const bandID = this.props.navigation.getParam('bandID', 'Error - Undefined Band ID');
        const bandMembers = this.props.navigation.getParam('bandMembers', 'Error - Unable to Obtain Band Members');
        this.ref = firebase.firestore().collection('Bands').doc(bandID);
        this.unsubscribe = null;

        this.state = {
            userData: bandMembers,
            users: [],
            bandID
        };
    }

    componentDidMount() {
        this.unsubscribe = this.ref.onSnapshot(this.onBandUpdate);
    }

    componentWillUnmount() {
        this.unsubscribe();
    }

    onBandUpdate = (querySnapshot) => {
        const band = querySnapshot.data();
        const { users } = band;
        const { userData } = this.state;
        const usersArray = [];

        Object.keys(users).forEach((user) => {
            if (userData[user]) {
                const name = userData[user].name;
                let displayName = name;
                if (name == null || name === undefined || name === '') {
                    displayName = 'Unnamed Band Member';
                }
                usersArray.push({
                    key: user,
                    name: displayName,
                    writePermission: users[user].writePermission
                });
            }
        });

        this.setState({
            users: usersArray,
        });
    }

    onInviteBandMemberPress = () => {
        this.props.navigation.navigate('InviteBandMember', { bandID: this.state.bandID });
    }

    renderUserList() {
        if (this.state.users.length > 0) {
            return (
                <FlatList
                    data={this.state.users}
                    extraData={this.state}
                    renderItem={
                        ({ item }) => (
                            <SimpleTableItem text={item.name} />
                        )
                    }
                />
            );
        }
        return <Text>There are no members in this band.</Text>;
    }

    render() {
        return (
            <CustomContainer style={styles.container} barStyle="default">
                <SectionHeader text="Band Members" />
                {this.renderUserList()}

                {/* INVITE BAND MEMBER BUTTON */}
                <View style={styles.inviteBandMemberButtonView}>
                    <View style={styles.inviteBandMemberButton}>
                        <WideButton text="Invite Band Member" onPress={this.onInviteBandMemberPress} color={lightGreen} />
                    </View>
                </View>
            </CustomContainer>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 0,
        height: 40
    },
    inviteBandMemberButtonView: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    inviteBandMemberButton: {
        width: '80%',
    }
});

const mapStateToProps = state => ({ isLoading: state.isLoading });

export default connect(mapStateToProps, { setIsLoading })(BandDetail);

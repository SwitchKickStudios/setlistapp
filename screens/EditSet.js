import React, { Component } from 'react';
import { Content, Form } from 'native-base';
import {
    Alert,
    Keyboard,
    Text,
    StyleSheet,
    View
} from 'react-native';
import Moment from 'moment';
import { connect } from 'react-redux';
import { setIsLoading } from '../redux/app-redux';
import SetsService from '../services/setsService';
import CustomContainer from '../components/customContainer';
import FormTextInput from '../components/formTextInput';
import WideButton from '../components/wideButton';
import DatePicker from '../components/datePicker';
import globalStyles from '../constants/globalStyles';
import { secondaryColor } from '../constants/styleVariables';
import * as globalConstants from '../constants/globalConstants';

class EditSet extends Component {
    constructor(props) {
        super(props);

        const setName = this.props.navigation.getParam('setName', 'Error - Set Name Not Found');
        const setDate = this.props.navigation.getParam('setDate', 'Error - Set Date Not Found');
        const bandID = this.props.navigation.getParam('bandID', 'Error - Set Name Not Found');
        const setID = this.props.navigation.getParam('setID', 'Error - Set Name Not Found');

        Moment.locale('en');
        const formattedDate = Moment(setDate).format('MMMM D, YYYY');

        this.state = {
            newSetName: setName,
            bandID,
            setID,
            setDate,
            setDateDisplay: formattedDate
        };
    }

    submitSet = async () => {
        let alertMessage = 'Error - Problem Renaming Set';
        const setsService = new SetsService();
        const {
            newSetName,
            bandID,
            setID,
            setDate
        } = this.state;
        this.props.setIsLoading(true);
        try {
            const result = await setsService.editSet(bandID, setID, newSetName, setDate);
            if (result.success) {
                this.props.navigation.goBack();
                return;
            }
            alertMessage = result.message;
        } catch {
            alertMessage = 'Error - Problem Renaming Set';
        }
        this.props.setIsLoading(false);
        Alert.alert(alertMessage);
    }

    updateSetNameTextInput = (newSetName) => {
        this.setState({ newSetName });
    }

    onConfirm = (date) => {
        Moment.locale('en');
        const formattedDate = Moment(date).format('MMMM D, YYYY');
        this.setState({ setDate: date, setDateDisplay: `${formattedDate}` });
    }

    onOpenDatePicker = () => {
        Keyboard.dismiss();
    }


    render() {
        return (
            <CustomContainer style={styles.container} barStyle="default">
                <Content contentContainerStyle={{ flexGrow: 1 }} style={{ flexGrow: 1 }} scrollEnabled={false}>
                    <View style={globalStyles.centeredFormContainer}>
                        <Form style={globalStyles.columnForm}>
                            {/* LABEL */}
                            <Text style={styles.label}>Edit set:</Text>

                            {/* SET Name INPUT */}
                            <FormTextInput
                                label="Enter a new name for your set"
                                value={this.state.newSetName}
                                onChangeText={text => this.updateSetNameTextInput(text)}
                                keyboardType="default"
                                autoCapitalize="sentences"
                                multiline
                                maxLength={globalConstants.setMax}
                            />

                            <DatePicker
                                onConfirm={this.onConfirm}
                                onOpenDatePicker={this.onOpenDatePicker}
                                value={this.state.setDateDisplay}
                                date={this.state.setDate}
                            />

                            {/* CREATE SET BUTTON */}
                            <WideButton text="Rename Set" onPress={this.submitSet} color={secondaryColor} />
                        </Form>
                    </View>

                    <View style={{ height: '25%' }} />
                </Content>
            </CustomContainer>

        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    label: {
        color: secondaryColor,
        fontSize: 28,
        paddingLeft: 3,
    },
    setNameLabel: {
        fontSize: 28,
        textAlign: 'center'
    },
});

const mapStateToProps = state => ({ isLoading: state.isLoading });

export default connect(mapStateToProps, { setIsLoading })(EditSet);

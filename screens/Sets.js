import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    FlatList,
    TouchableOpacity,
    Alert,
    View
} from 'react-native';
import { Icon } from 'native-base';
import * as firebase from 'firebase';
import { connect } from 'react-redux';
import {
    setUserDisplayName,
    setIsLoading,
    setSetsShowDate,
    setSetsSortMode
} from '../redux/app-redux';
import BandService from '../services/bandService';
import SetsTableItem from '../components/setsTableItem';
import EmptyContentMessage from '../components/emptyContentMessage';
import { secondaryColor } from '../constants/styleVariables';
import globalStyles from '../constants/globalStyles';
import SetsService from '../services/setsService';
import {
    sortDateAsc,
    sortDateDesc,
    sortNameAsc,
    sortNameDesc
} from '../utilities/sortFunctions';
import ProfileDrawerButton from '../components/profileDrawerButton';
import ExtendedHeader from '../components/extendedHeader';
import CustomContainer from '../components/customContainer';
import NavigationAwareFAB from '../components/navigationAwareFAB';
import ShowHideButton from '../components/showHideButton';
import SortButton from '../components/sortButton';

class Sets extends Component {
    static navigationOptions = ({ navigation }) => {
        const { params } = navigation.state;
        return {
            headerBackTitle: 'Back',
            headerLeft: () => (
                !params.showDoneButton ? (
                    <View style={styles.headerLeft}>
                        <TouchableOpacity onPress={() => params.bandMenuButtonAction()}>
                            <Icon name="menu" type="MaterialCommunityIcons" style={{ fontSize: 24, color: secondaryColor, paddingLeft: 12 }} />
                        </TouchableOpacity>
                    </View>
                ) : null
            ),
            title: params ? params.screenTitle : 'Sets',
            headerRight: () => (
                params && params.showDoneButton ? (
                    <TouchableOpacity onPress={() => params.doneButtonAction()}>
                        <Text style={[globalStyles.headerButtonText, globalStyles.closeButtonText]}>Close</Text>
                    </TouchableOpacity>
                ) : (
                    <ProfileDrawerButton onPress={() => navigation.openDrawer()} />
                )
            )
        };
    }

    constructor(props) {
        super(props);

        const bandID = this.props.navigation.getParam('bandID', 'Error - Undefined Band ID');
        const user = firebase.auth().currentUser;

        this.ref = firebase.firestore().collection('Bands').doc(bandID).collection('Sets');
        this.bandRef = firebase.firestore().collection('Bands').doc(bandID);
        this.userRef = firebase.firestore().collection('Users').doc(`${user.uid}`);

        this.unsubscribe = null;
        this.bandUnsubstribe = null;
        this.userUnsubscribe = null;

        this.state = {
            bandID,
            bandName: '',
            sets: [],
            deleteMode: false,
            renameMode: false,
            FABOpen: false,
        };
    }

    componentDidMount() {
        this.props.setIsLoading(true);
        this.props.navigation.setParams({ doneButtonAction: this.doneButtonAction });
        this.props.navigation.setParams({ bandMenuButtonAction: this.bandMenuButtonAction });
        this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
        this.bandUnsubstribe = this.bandRef.onSnapshot(this.onBandUpdate);
        this.userUnsubscribe = this.userRef.get().then(this.checkUserData);
    }

    componentWillUnmount() {
        this.unsubscribe();
        this.bandUnsubstribe();
    }

    onCollectionUpdate = (querySnapshot) => {
        const sets = [];
        querySnapshot.forEach((doc) => {
            const { date, name } = doc.data();
            sets.push({
                key: doc.id, // Document ID
                name,
                date
            });
        });

        this.setState({ sets });
        this.props.setIsLoading(false);

        if (this.state.sets.length < 1) {
            this.doneButtonAction();
        }
    }

    onBandUpdate = (querySnapshot) => {
        if (querySnapshot.exists) {
            const bandName = querySnapshot.data().bandData.name;
            this.setState({ bandName });
            this.props.navigation.setParams({ screenTitle: bandName });
        }
    }

    getSetsShowDate() {
        const { setsShowDate } = this.props;
        return setsShowDate;
    }

    getSetsSortMode() {
        const { setsSortMode } = this.props;
        return setsSortMode;
    }

    onShowDatePress = () => {
        const nextDateShowHide = !this.getSetsShowDate();
        this.props.setSetsShowDate(nextDateShowHide);
    }

    onNameAscPress = () => {
        this.props.setSetsSortMode('nameAsc');
    }

    onNameDescPress = () => {
        this.props.setSetsSortMode('nameDesc');
    }

    onDateAscPress = () => {
        this.props.setSetsSortMode('dateAsc');
    }

    onDateDescPress = () => {
        this.props.setSetsSortMode('dateDesc');
    }

    // Method for handing the delete buttons on the FlatList items
    onDeleteSet = (bandID, setID, setName) => {
        Alert.alert(
            `Are you sure you want to delete ${setName}?`,
            "All of this set's song data will be erased. This action is not reversible.",
            [
                { text: 'Cancel', onPress: () => console.log('Cancelled - Leaved Band') },
                { text: 'OK', onPress: () => this.deleteSet(bandID, setID) },
            ],
            { cancelable: false }
        );
    }

    checkUserData = (querySnapshot) => {
        if (querySnapshot.exists) {
            const displayName = querySnapshot.data().name;
            this.props.setUserDisplayName(displayName);
        } else {
            this.props.navigation.navigate('CreateUser');
        }
    }

    // Use SetsService to delete Set from Band
    deleteSet = async (bandID, setID) => {
        const setsService = new SetsService();
        let alertMessage = 'Error - Problem Deleting Set';
        this.props.setIsLoading(true);
        try {
            const result = await setsService.deleteSet(bandID, setID);
            alertMessage = result.message;
        } catch {
            alertMessage = 'Error - Problem Deleting Set';
        }
        this.props.setIsLoading(false);
        Alert.alert(alertMessage);
    }

    onCreateSetPress = () => {
        this.props.navigation.navigate('CreateSet', { bandID: this.state.bandID });
    }

    bandMenuButtonAction = async () => {
        let alertMessage = 'Error - Problem Retrieving Band Details';
        const bandService = new BandService();
        const bandID = this.state.bandID;

        this.props.setIsLoading(true);
        try {
            const result = await bandService.getBandDetails(bandID);
            if (result.success) {
                this.props.setIsLoading(false);
                const responeJSON = result.result;
                const responseData = JSON.parse(responeJSON);
                const bandMembers = {};
                responseData.forEach((user) => {
                    const { id, name } = user;
                    bandMembers[id] = { name };
                });
                this.props.navigation.navigate('BandDetail', { bandID: this.state.bandID, bandName: this.state.bandName, bandMembers });
                return;
            }
            alertMessage = result.message;
        } catch (responseError) {
            alertMessage = `Error - Unable to Retrieve Band Details: ${responseError}`;
        }
        this.props.setIsLoading(false);
        Alert.alert(alertMessage);
    }

    onSetSelect = (bandID, setID, setName, date) => {
        this.props.navigation.navigate('Songs', {
            bandID,
            setID,
            setName,
            date
        });
    }

    // Method for the Delete Set button - toggle Delete Mode
    onDeleteSetPress = () => {
        this.props.navigation.setParams({ showDoneButton: true });
        this.setState({ deleteMode: true });
    }

    // Header button action for turning off deleteMode
    doneButtonAction = () => {
        this.setState({ deleteMode: false, renameMode: false });
        this.props.navigation.setParams({ showDoneButton: false });
    }

    renderSetList() {
        if (this.state.sets.length > 0) {
            let sortedSets = this.state.sets;
            if (this.getSetsSortMode() === 'dateAsc') {
                sortedSets = this.state.sets.sort(sortDateAsc);
            } else if (this.getSetsSortMode() === 'dateDesc') {
                sortedSets = this.state.sets.sort(sortDateDesc);
            } else if (this.getSetsSortMode() === 'nameDesc') {
                sortedSets = this.state.sets.sort(sortNameDesc);
            } else {
                sortedSets = this.state.sets.sort(sortNameAsc);
            }

            return (
                <FlatList
                    data={sortedSets}
                    extraData={[this.state, this.getSetsShowDate()]}
                    style={globalStyles.fullscreenFlatList}
                    renderItem={
                        ({ item }) => (
                            <SetsTableItem
                                onPress={() => this.onSetSelect(this.state.bandID, item.key, item.name, item.date)}
                                onDelete={() => this.onDeleteSet(this.state.bandID, item.key, item.name)}
                                text={item.name}
                                date={item.date}
                                deleteMode={this.state.deleteMode}
                                renameMode={this.state.renameMode}
                                showDate={this.getSetsShowDate()}
                                isSelected
                            />
                        )
                    }
                />
            );
        }
        return <EmptyContentMessage message="This band doesn't have any setlists yet." />;
    }

    renderActionButton(actions) {
        return (
            <NavigationAwareFAB
                open={this.state.FABOpen}
                onStateChange={({ open }) => this.setState({ FABOpen: open })}
                actions={actions}
                icon={({ size, color }) => (<Icon name="queue-music" type="MaterialIcons" style={{ fontSize: size, color }} />)}
            />
        );
    }

    renderBottomButtons() {
        if (!this.state.deleteMode && !this.state.renameMode) {
            if (this.state.sets.length < 1) {
                const actions = [
                    {
                        icon: (({ color }) => (
                            <Icon name="playlist-add" type="MaterialIcons" style={{ fontSize: 25, color }} />
                        )),
                        label: 'Create Set',
                        onPress: () => this.onCreateSetPress()
                    },
                ];
                return this.renderActionButton(actions);
            }
            const actions = [
                {
                    icon: (({ color }) => (
                        <Icon name="playlist-add" type="MaterialIcons" style={{ fontSize: 26, color }} />
                    )),
                    label: 'Create Set',
                    onPress: () => this.onCreateSetPress()
                },
                {
                    icon: (({ size, color }) => (
                        <Icon name="delete-sweep" type="MaterialIcons" style={{ fontSize: size, color }} />
                    )),
                    label: 'Delete Set',
                    onPress: () => this.onDeleteSetPress()
                },
            ];
            return this.renderActionButton(actions);
        }
        return null;
    }

    render() {
        return (
            <CustomContainer barStyle="default" hideStatusBar={this.props.navigation.state.isDrawerOpen}>
                <ExtendedHeader style={globalStyles.extendedHeader}>
                    <Text style={globalStyles.extendedHeaderTitle}>Sets</Text>
                    {/* TODO: REMOVE OPACITY TO SHOW THESE BUTTONS */}
                    <View style={[globalStyles.extendedHeaderToolbar]}>
                        <ShowHideButton onShowDatePress={this.onShowDatePress} showDate={this.getSetsShowDate()} />
                        <SortButton
                            onNameAscPress={this.onNameAscPress}
                            onNameDescPress={this.onNameDescPress}
                            onDateAscPress={this.onDateAscPress}
                            onDateDescPress={this.onDateDescPress}
                        />
                    </View>
                </ExtendedHeader>
                {this.renderSetList()}
                {this.renderBottomButtons()}
            </CustomContainer>
        );
    }
}

const styles = StyleSheet.create({
    headerLeft: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
    hideIcon: {
        fontSize: 24,
        color: secondaryColor,
        right: 8,
    },
    searchIcon: {
        fontSize: 24,
        color: secondaryColor,
        right: 8,
        bottom: 2
    },
});

const mapStateToProps = state => ({
    userDisplayName: state.userDisplayName,
    isLoading: state.isLoading,
    setsShowDate: state.setsShowDate,
    setsSortMode: state.setsSortMode
});
export default connect(mapStateToProps, {
    setUserDisplayName,
    setIsLoading,
    setSetsShowDate,
    setSetsSortMode
})(Sets);

import React, { Component } from 'react';
import { Content, Form } from 'native-base';
import {
    Alert,
    Keyboard,
    Text,
    StyleSheet,
    View
} from 'react-native';
import Moment from 'moment';
import { connect } from 'react-redux';
import { setIsLoading } from '../redux/app-redux';
import CustomContainer from '../components/customContainer';
import RightHeaderText from '../components/rightHeaderText';
import WideButton from '../components/wideButton';
import globalStyles from '../constants/globalStyles';
import FormTextInput from '../components/formTextInput';
import DatePicker from '../components/datePicker';
import { secondaryColor } from '../constants/styleVariables';
import SongsService from '../services/songsService';
import * as globalConstants from '../constants/globalConstants';

class CreateSong extends Component {
    static navigationOptions = {
        headerRight: () => (
            <RightHeaderText text="New Song" />
        ),
    };

    constructor(props) {
        super(props);
        Moment.locale('en');
        const currentDate = new Date();
        const formattedDate = Moment(currentDate).format('MMMM D, YYYY');

        const bandID = this.props.navigation.getParam('bandID', 'Error - Undefined Band ID');
        const setID = this.props.navigation.getParam('setID', 'Error - Undefined Set ID');
        this.state = {
            bandID,
            setID,
            songName: '',
            artist: '',
            tempo: '',
            musicalKey: '',
            songDate: currentDate,
            songDateDisplay: formattedDate
        };
    }

    // Create Song Button action
    submitSong = async () => {
        let alertMessage = 'Error - Problem Creating Song';
        const songsService = new SongsService();
        const {
            bandID,
            setID,
            songName,
            artist,
            tempo,
            musicalKey,
            songDate
        } = this.state;

        if (songDate.toString().length < 1) {
            Alert.alert('Invalid Song Date', 'Date can\'t be empty.');
            return;
        }

        this.props.setIsLoading(true);
        try {
            const result = await songsService.createNewSong(bandID, setID, songName, artist, tempo, musicalKey, songDate);
            if (result.success) {
                this.props.navigation.navigate('Songs', {
                    bandID,
                    setID,
                    setName: this.props.navigation.getParam('setName', 'Error - Undefined Set Name')
                });
                return;
            }
            alertMessage = result.message;
        } catch {
            alertMessage = 'Error - Problem Creating Song';
        }
        this.props.setIsLoading(false);
        Alert.alert(alertMessage);
    }

    updateArtistNameTextInput = (artist) => {
        this.setState({ artist });
    }

    onConfirm = (date) => {
        Moment.locale('en');
        const formattedDate = Moment(date).format('MMMM D, YYYY');
        this.setState({ songDate: date, songDateDisplay: `${formattedDate}` });
    }

    onOpenDatePicker = () => {
        Keyboard.dismiss();
    }

    render() {
        return (
            <CustomContainer style={styles.container} barStyle="default">
                <Content contentContainerStyle={{ flexGrow: 1 }} style={{ flexGrow: 1 }} scrollEnabled={false}>
                    <View style={styles.formContainer}>
                        <Form style={globalStyles.columnForm}>
                            {/* LABEL */}
                            <Text style={styles.label}>New Song:</Text>

                            <FormTextInput
                                label="Song name:"
                                value={this.state.songName}
                                onChangeText={songName => this.setState({ songName })}
                                multiline
                                maxLength={globalConstants.songNameMax}
                                autoCapitalize="sentences"
                            />
                            <FormTextInput
                                label="Artist:"
                                value={this.state.artist}
                                onChangeText={artist => this.setState({ artist })}
                                multiline
                                maxLength={globalConstants.artistMax}
                                autoCapitalize="sentences"
                            />
                            <View>
                                <FormTextInput
                                    label="Tempo:"
                                    value={this.state.tempo}
                                    onChangeText={tempo => this.setState({ tempo })}
                                    maxLength={globalConstants.tempoMax}
                                />
                                <View style={styles.bpmView}>
                                    <Text style={styles.bpmLabel}>bpm</Text>
                                </View>
                            </View>
                            <FormTextInput
                                label="Key:"
                                value={this.state.musicalKey}
                                onChangeText={musicalKey => this.setState({ musicalKey })}
                                maxLength={globalConstants.musicalKeyMax}
                                autoCapitalize="sentences"
                            />
                            <DatePicker
                                onConfirm={this.onConfirm}
                                onOpenDatePicker={this.onOpenDatePicker}
                                value={this.state.songDateDisplay}
                            />

                            {/* CREATE SET BUTTON */}
                            <WideButton text="Create Song" onPress={this.submitSong} color={secondaryColor} buttonStyle={styles.createSongButton} />
                        </Form>
                    </View>
                </Content>
            </CustomContainer>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    formContainer: {
        paddingTop: 40,
        alignItems: 'center',
    },
    createSongButton: {
        marginTop: 20
    },
    bpmView: {
        marginTop: 20,
        paddingRight: 12,
        position: 'absolute',
        right: 0,
        height: '100%',
        justifyContent: 'center'
    },
    bpmLabel: {
        textAlign: 'right',
        color: 'gray',
    },
    label: {
        color: secondaryColor,
        fontSize: 28,
        paddingLeft: 3
    },
});

const mapStateToProps = state => ({ isLoading: state.isLoading });
export default connect(mapStateToProps, { setIsLoading })(CreateSong);

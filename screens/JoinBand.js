import React from 'react';
import {
    Alert,
    StyleSheet,
    View,
    Text,
} from 'react-native';
import { Content, Form } from 'native-base';
import { connect } from 'react-redux';
import { setIsLoading } from '../redux/app-redux';
import CustomContainer from '../components/customContainer';
import RightHeaderText from '../components/rightHeaderText';
import FormTextInput from '../components/formTextInput';
import WideButton from '../components/wideButton';
import globalStyles from '../constants/globalStyles';
import { secondaryColor } from '../constants/styleVariables';
import BandMemberService from '../services/bandMemberService';

class JoinBand extends React.Component {
    static navigationOptions = () => ({
        headerRight: () => (
            <RightHeaderText text="Join a Band" />
        ),
    });

    constructor(props) {
        super(props);

        const invitationCode = this.props.navigation.getParam('invitationCode', '');

        this.state = {
            code: invitationCode,
        };
    }

    onSubmit = async () => {
        if (this.state.code.length < 1) {
            Alert.alert('Invalid Invitation Code', 'Invitation code can\'t be empty.');
            return;
        }

        const bandMemberService = new BandMemberService();
        this.props.setIsLoading(true);

        try {
            const result = await bandMemberService.submitInvitationCode(this.state.code);
            this.props.setIsLoading(false);
            Alert.alert(result.message);
            if (result.success) {
                this.props.navigation.navigate('Bands');
            }
        } catch {
            this.props.setIsLoading(false);
            Alert.alert('Error', 'Problem Joining Band.');
        }
    }

    render() {
        return (
            <CustomContainer style={styles.container} barStyle="default">
                <Content contentContainerStyle={{ flexGrow: 1 }} style={{ flexGrow: 1 }} scrollEnabled={false}>
                    <View style={globalStyles.centeredFormContainer}>
                        <Form style={styles.form}>
                            {/* LABEL */}
                            <Text style={styles.label}>Enter Invitation Code:</Text>

                            {/* INVITATION CODE INPUT */}
                            <FormTextInput
                                label="Invitation Code"
                                value={this.state.code}
                                onChangeText={(text) => { this.setState({ code: text }); }}
                                keyboardType="default"
                            />
                            {/* CREATE ACCOUNT BUTTON */}
                            <WideButton text="Join Band" onPress={this.onSubmit} color={secondaryColor} />
                        </Form>
                    </View>

                    <View style={{ height: '25%' }} />
                </Content>
            </CustomContainer>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    form: {
        width: '80%',
        flexDirection: 'column',
        justifyContent: 'center',
    },
    label: {
        color: secondaryColor,
        fontSize: 28,
        paddingLeft: 2
    },
});

const mapStateToProps = state => ({ isLoading: state.isLoading });

export default connect(mapStateToProps, { setIsLoading })(JoinBand);

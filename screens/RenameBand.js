import React, { Component } from 'react';
import { Content, Form } from 'native-base';
import {
    Alert,
    Text,
    StyleSheet,
    View
} from 'react-native';
import { connect } from 'react-redux';
import { setIsLoading } from '../redux/app-redux';
import BandService from '../services/bandService';
import CustomContainer from '../components/customContainer';
import FormTextInput from '../components/formTextInput';
import WideButton from '../components/wideButton';
import globalStyles from '../constants/globalStyles';
import { secondaryColor } from '../constants/styleVariables';
import * as globalConstants from '../constants/globalConstants';

class RenameBand extends Component {
    constructor(props) {
        super(props);

        const bandName = this.props.navigation.getParam('bandName', 'Error - Band Name Not Found');
        const bandID = this.props.navigation.getParam('bandID', 'Error - Band Name Not Found');

        this.state = {
            previousBandName: bandName,
            newBandName: bandName,
            bandID,
        };
    }

    submitBand = async () => {
        let alertMessage = 'Error - Problem Renaming Band';
        const bandService = new BandService();
        const { newBandName, bandID } = this.state;
        this.props.setIsLoading(true);
        try {
            const result = await bandService.renameBand(bandID, newBandName);
            if (result.success) {
                this.props.navigation.goBack();
                return;
            }
            alertMessage = result.message;
        } catch {
            alertMessage = 'Error - Problem Renaming Band';
        }
        this.props.setIsLoading(false);
        Alert.alert(alertMessage);
    }

    updateBandNameTextInput = (newBandName) => {
        this.setState({ newBandName });
    }

    render() {
        return (
            <CustomContainer style={styles.container} barStyle="default">
                <Content contentContainerStyle={{ flexGrow: 1 }} style={{ flexGrow: 1 }} scrollEnabled={false}>
                    <Text style={styles.setNameLabel}>{this.state.previousBandName}</Text>

                    <View style={globalStyles.centeredFormContainer}>
                        <Form style={globalStyles.columnForm}>
                            {/* LABEL */}
                            <Text style={styles.label}>Rename band:</Text>

                            {/* SET Name INPUT */}
                            <FormTextInput
                                label="Enter a new name for your band"
                                value={this.state.newBandName}
                                onChangeText={text => this.updateBandNameTextInput(text)}
                                keyboardType="default"
                                autoCapitalize="sentences"
                                maxLength={globalConstants.artistMax}
                            />

                            {/* CREATE SET BUTTON */}
                            <WideButton text="Rename Band" onPress={this.submitBand} color={secondaryColor} />
                        </Form>
                    </View>

                    <View style={{ height: '25%' }} />
                </Content>
            </CustomContainer>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    label: {
        color: secondaryColor,
        fontSize: 28,
        paddingLeft: 3,
    },
    setNameLabel: {
        fontSize: 28,
        textAlign: 'center'
    },
});

const mapStateToProps = state => ({ isLoading: state.isLoading });

export default connect(mapStateToProps, { setIsLoading })(RenameBand);

import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    Text,
    View,
    StyleSheet,
} from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';
import { initializeState } from '../redux/app-redux';
import AppLogo from '../components/appLogo';
import BandMemberService from '../services/bandMemberService';

class SplashScreen extends Component {
    static navigationOptions = () => ({ headerShown: null })

    componentDidMount() {
        this.props.initializeState();
        this.navigateToInitialScreen(this.getCurrentBandID());
    }

    getCurrentBandID() {
        const { currentBandID } = this.props;
        return currentBandID;
    }

    // If we have a band saved in out persisted REDUX state,
    // We try to get the bandName from FIRESTORE
    // If we cannot get bandName or do not have a saved band,
    // navigate to BANDS screen
    async navigateToInitialScreen() {
        if (this.props.navigation) {
            const currentBandID = this.getCurrentBandID();
            const bandMemberService = new BandMemberService();
            let shouldNavigateToSets = false;

            try {
                const result = await bandMemberService.checkBandAuth(currentBandID);
                console.log(result);
                shouldNavigateToSets = result.authorized;
            } catch {
                shouldNavigateToSets = false;
            }

            if (shouldNavigateToSets) {
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'InitialSets', params: { bandID: currentBandID } })]
                });
                this.props.navigation.dispatch(resetAction);
            } else {
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [
                        NavigationActions.navigate({ routeName: 'Bands', params: {} })
                    ]
                });
                this.props.navigation.dispatch(resetAction);
            }
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.loadingTextContainer}>
                    <AppLogo />
                </View>
                <View style={styles.loadingTextContainer}>
                    <Text>Loading</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    loadingTextContainer: {
        paddingTop: 40,
    }
});

const mapStateToProps = state => ({ currentBandID: state.currentBandID });
export default connect(mapStateToProps, { initializeState })(SplashScreen);

import React from 'react';
import {
    // StyleSheet,
    View,
    Text,
    Button,
    TextInput,
    FlatList
} from 'react-native';
import * as firebase from 'firebase';
import { connect } from 'react-redux';
import { setFavoriteAnimal } from '../redux/app-redux';
// import { setFavoriteAnimal, watchPersonData } from '../redux/app-redux';

import 'firebase/firestore';

const mapStateToProps = state => ({
    favoriteAnimal: state.favoriteAnimal,
    personData: state.personData
});

const mapDispatchToProps = dispatch => ({
    setFavoriteAnimal: (text) => { dispatch(setFavoriteAnimal(text)); },
    // watchPersonData: () => { dispatch(watchPersonData()); }
});

class TestScreen extends React.Component {
    constructor(props) {
        super(props);
        this.ref = firebase.firestore().collection('posts');
        this.unsubscribe = null;

        this.state = {
            favoriteAnimal: this.props.favoriteAnimal,
            posts: [],
            song: '',
            artist: '',
            // loading: true
        };

        // this.props.watchPersonData();
    }

    componentDidMount() {
        this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
    }

    componentWillUnmount() {
        this.unsubscribe();
    }

    onCollectionUpdate = (querySnapshot) => {
        const posts = [];
        querySnapshot.forEach((doc) => {
            const { artist, song } = doc.data();
            posts.push({
                key: doc.id, // Document ID
                doc, // DocumentSnapshot
                artist,
                song,
            });
        });
        this.setState({
            posts,
            // loading: false,
        });
    }

    onSignOutPress = () => {
        firebase.auth().signOut();
    }

    onSetFavoriteAnimalPress = () => {
        this.props.setFavoriteAnimal(this.state.favoriteAnimal);
    }

    onAddSongPress = () => {
        this.ref.add({
            artist: this.state.artist,
            song: this.state.song
        });
    }

    renderArtistAndSongInfo() {
        if (this.state.posts.length > 0) {
            return (
                <FlatList
                    data={this.state.posts}
                    renderItem={
                        ({ item }) => <Text>{item.artist} - {item.song}</Text>
                    }
                />
            );
        }
        return <Text>No Songs Found</Text>;
    }

    render() {
        return (
            <View style={{ paddingTop: 20 }}>
                <Text style={{ paddingTop: 20 }}>{this.props.favoriteAnimal}</Text>
                <Button title="Sign Out" onPress={this.onSignOutPress} />

                <TextInput
                    style={{ borderWidth: 1, width: 200, height: 40 }}
                    value={this.state.favoriteAnimal}
                    onChangeText={(text) => { this.setState({ favoriteAnimal: text }); }}
                />
                <Button title="Set Favorite Animal" onPress={this.onSetFavoriteAnimalPress} />

                <Text>Add Song:</Text>
                <TextInput
                    style={{ borderWidth: 1, width: 200, height: 40 }}
                    value={this.state.song}
                    onChangeText={(text) => { this.setState({ song: text }); }}
                    placeholder="Song"
                />
                <TextInput
                    style={{ borderWidth: 1, width: 200, height: 40 }}
                    value={this.state.artist}
                    onChangeText={(text) => { this.setState({ artist: text }); }}
                    placeholder="Artist"
                />
                <Button title="Add Song" onPress={this.onAddSongPress} />

                <Text>{this.props.personData.firstName}</Text>
                <Text>{this.props.personData.lastName}</Text>

                <Text>Current Songs:</Text>
                {this.renderArtistAndSongInfo()}
            </View>
        );
    }
}

// const styles = StyleSheet.create({
//
// });

export default connect(mapStateToProps, mapDispatchToProps)(TestScreen);

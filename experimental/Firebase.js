import * as firebase from 'firebase';
// import firestore from 'firebase/firestore';

const settings = { timestampsInSnapshots: true };

const config = {
    apiKey: 'AIzaSyAFkaFFEObpkjRbUXIY_k3l7biTTxJ7O1w',
    authDomain: 'setlistapp-2cc45.firebaseapp.com',
    databaseURL: 'https://setlistapp-2cc45.firebaseio.com',
    projectId: 'setlistapp-2cc45',
    storageBucket: 'setlistapp-2cc45.appspot.com',
    messagingSenderId: '559077735318'
};
firebase.initializeApp(config);

firebase.firestore().settings(settings);

export default firebase;

import { createStackNavigator, TransitionPresets } from 'react-navigation-stack';
import CreateBandMember from '../screens/createBandMember';
import SplashScreen from '../screens/SplashScreen';
import Bands from '../screens/Bands';
import StartBand from '../screens/StartBand';
import RenameBand from '../screens/RenameBand';
import Sets from '../screens/Sets';
import CreateSet from '../screens/CreateSet';
import EditSet from '../screens/EditSet';
import InviteBandMember from '../screens/InviteBandMember';
import JoinBand from '../screens/JoinBand';
import Songs from '../screens/Songs';
import CreateSong from '../screens/CreateSong';
import SongDetail from '../screens/SongDetail';
import BandDetail from '../screens/BandDetail';
import UserProfile from '../screens/UserProfile';
import { secondaryColor } from '../constants/styleVariables';

const MainStackNavigation = createStackNavigator(
    {
        SplashScreen: { screen: SplashScreen },
        CreateUser: { screen: CreateBandMember },
        Bands: {
            screen: Bands,
            navigationOptions: TransitionPresets.ModalTransition
        },
        StartBand: { screen: StartBand },
        RenameBand: { screen: RenameBand },
        Sets: { screen: Sets },
        InitialSets: {
            screen: Sets,
            navigationOptions: TransitionPresets.ModalTransition
        },
        CreateSet: { screen: CreateSet },
        EditSet: {
            screen: EditSet,
            navigationOptions: TransitionPresets.ModalSlideFromBottomIOS
        },
        InviteBandMember: { screen: InviteBandMember },
        JoinBand: { screen: JoinBand },
        Songs: { screen: Songs },
        CreateSong: { screen: CreateSong },
        SongDetail: { screen: SongDetail },
        BandDetail: {
            screen: BandDetail,
            navigationOptions: TransitionPresets.ModalSlideFromBottomIOS
        },
        UserProfile: { screen: UserProfile }
    },
    {
        initialRouteName: 'SplashScreen',
        headerMode: 'float',
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: 'white',
                borderBottomWidth: 0,
            },
            headerTintColor: secondaryColor,
            headerTitleStyle: {
                fontWeight: 'normal',
                fontSize: 18,
            },
        },
    }
);

export default MainStackNavigation;

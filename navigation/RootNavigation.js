import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';

import LoginScreen from '../screens/auth/LoginScreen';
import SignUpScreen from '../screens/auth/SignUpScreen';
import ForgotPasswordScreen from '../screens/auth/ForgotPasswordScreen';

import { primaryColor } from '../constants/styleVariables';

const RootStackNavigator = createStackNavigator(
    {
        Login: { screen: LoginScreen },
        SignUp: { screen: SignUpScreen },
        ForgotPassword: { screen: ForgotPasswordScreen }
    },
    {
        defaultNavigationOptions: {
            headerStyle: {
                // backgroundColor: primaryColor,
                borderBottomWidth: 0,
            },
            headerTintColor: primaryColor,
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        },
        initialRouteName: 'Login'
    }
);

const RootStackNavigatorWithSafeArea = createAppContainer(RootStackNavigator);

export default class RootNavigation extends React.Component {
    render() {
        return <RootStackNavigatorWithSafeArea />;
    }
}

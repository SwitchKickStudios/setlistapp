import React from 'react';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createAppContainer } from 'react-navigation';
import SideBar from '../components/sideBar';
import MainStackNavigation from './MainStackNavigation';

const MainDrawerNavigator = createDrawerNavigator(
    {
        MainStackNavigation: { screen: MainStackNavigation }
    },
    {
        contentComponent: props => <SideBar {...props} />
    }
);

const MainDrawerNavigatorWithSafeArea = createAppContainer(MainDrawerNavigator);

export default class BandsDrawerNavigation extends React.Component {
    constructor(props) {
        super(props);

        this.navigationRef = React.createRef();
    }

    render() {
        return <MainDrawerNavigatorWithSafeArea ref={(navigatorRef) => { this.navigationRef = navigatorRef; }} />;
    }
}

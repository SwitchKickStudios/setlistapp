import axios from 'axios';
import getAuthHeaders from '../utilities/getAuthHeaders';
import { createBandENDPOINT, getBandDetailsENDPOINT, renameBandENDPOINT } from '../constants/Endpoints';

export default class BandService {
    constructor() {
        this.returnObject = {};
        this.responseData = {};
    }

    async createNewBand(bandName, uid) {
        // Validate song name
        if (bandName.trim().length < 1) {
            this.responseData = {
                success: false,
                message: 'Please enter a band name.',
            };
            return this.responseData;
        }

        // Try network post to create song document with arguments
        try {
            const headers = await getAuthHeaders();
            const response = await axios.post(createBandENDPOINT, {
                bandName,
                uid,
            }, { headers });
            this.responseData.success = response.data.success;
            this.responseData.message = response.data.message;
            return this.responseData;
        } catch (error) {
            this.responseData = {
                success: false,
                message: 'Problem Connecting with the Database.',
                error
            };
            return this.responseData;
        }
    }

    async getBandDetails(bandID) {
        try {
            const headers = await getAuthHeaders();
            const response = await axios.post(getBandDetailsENDPOINT, { bandID }, { headers });
            this.responseData.success = response.data.success;
            this.responseData.message = response.data.message;
            this.responseData.result = response.data.result;
            return this.responseData;
        } catch (error) {
            this.responseData = {
                success: false,
                message: 'Problem Connecting with the Database.',
                error
            };
            return this.responseData;
        }
    }

    async renameBand(bandID, newBandName) {
        try {
            const headers = await getAuthHeaders();
            const response = await axios.post(renameBandENDPOINT, { bandID, newBandName }, { headers });
            this.responseData.success = response.data.success;
            this.responseData.message = response.data.message;
            return this.responseData;
        } catch (error) {
            this.responseData = {
                success: false,
                message: 'Problem Connecting with the Database.',
                error
            };
            return this.responseData;
        }
    }
}

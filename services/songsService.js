import axios from 'axios';
import getAuthHeaders from '../utilities/getAuthHeaders';
import { deleteBatchSongsENDPOINT, createSongENDPOINT } from '../constants/Endpoints';

export default class SongsService {
    constructor() {
        this.returnObject = {};
        this.responseData = {};
    }

    async deleteBatchSongs(bandID, setID, songIDs) {
        try {
            const headers = await getAuthHeaders();
            const response = await axios.post(deleteBatchSongsENDPOINT, { bandID, setID, songIDs }, { headers });
            this.responseData.success = response.data.success;
            this.responseData.message = response.data.message;
            return this.responseData;
        } catch (error) {
            this.responseData = {
                success: false,
                message: 'Problem Connecting with the Database.',
                error
            };
            return this.responseData;
        }
    }

    async createNewSong(bandID, setID, songName, artist, tempo, musicalKey, songDate) {
        // Validate song name
        if (songName.trim().length < 1) {
            this.responseData = {
                success: false,
                message: 'Please enter a song name.',
            };
            return this.responseData;
        }

        // Try network post to create song document with arguments
        try {
            const headers = await getAuthHeaders();
            const response = await axios.post(createSongENDPOINT, {
                bandID,
                setID,
                songName,
                artist,
                tempo,
                musicalKey,
                songDate
            }, { headers });
            this.responseData.success = response.data.success;
            this.responseData.message = response.data.message;
            return this.responseData;
        } catch (error) {
            this.responseData = {
                success: false,
                message: 'Problem Connecting with the Database.',
                error
            };
            return this.responseData;
        }
    }
}

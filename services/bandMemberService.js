import axios from 'axios';
import * as firebase from 'firebase';
import getAuthHeaders from '../utilities/getAuthHeaders';
import {
    checkBandAuthENDPOINT,
    inviteBandMemberENDPOINT,
    submitInvitationCodeENDPOINT,
    createUserENDPOINT,
    deleteBandMemberENDPOINT
} from '../constants/Endpoints';

export default class BandMemberService {
    constructor() {
        this.returnObject = {};
        this.responseData = {};
        this.bandRef = null;
    }

    async checkBandAuth(bandID) {
        try {
            const headers = await getAuthHeaders();
            const response = await axios.post(checkBandAuthENDPOINT, { bandID }, { headers });
            this.responseData.authorized = response.data.authorized;
            this.responseData.error = response.data.error;
            return this.responseData;
        } catch (error) {
            this.responseData.error = error;
            this.responseData.authorized = false;
            return error;
        }
    }

    async getBandName(bandID) {
        try {
            this.bandRef = firebase.firestore().collection('Bands').doc(bandID);
            const response = await this.bandRef.get();
            this.responseData.bandName = response.data().bandData.name;
            this.responseData.success = true;
            return this.responseData;
        } catch (error) {
            this.responseData.error = error;
            this.responseData.success = false;
            return error;
        }
    }

    async inviteBandMember(phonenumber, bandID) {
        try {
            const headers = await getAuthHeaders();
            const response = await axios.post(inviteBandMemberENDPOINT, {
                phonenumber,
                bandID
            }, { headers });
            this.responseData.success = response.data.success;
            this.responseData.message = response.data.message;
            return this.responseData;
        } catch (error) {
            this.responseData = {
                success: false,
                message: 'Problem Connecting with the Database.',
                error
            };
            return this.responseData;
        }
    }

    async submitInvitationCode(invitationCode) {
        try {
            const headers = await getAuthHeaders();
            const response = await axios.post(submitInvitationCodeENDPOINT, {
                code: invitationCode,
                uid: firebase.auth().currentUser.uid
            }, { headers });
            this.responseData.success = response.data.success;
            this.responseData.message = response.data.message;
            this.responseData.bandname = response.data.bandname;
            return this.responseData;
        } catch (error) {
            this.responseData = {
                success: false,
                message: 'Problem Connecting with the Database.',
                error
            };
            return this.responseData;
        }
    }

    async createUser(displayName) {
        try {
            const headers = await getAuthHeaders();
            const response = await axios.post(createUserENDPOINT, { displayName }, { headers });
            this.responseData.success = response.data.success;
            this.responseData.message = response.data.message;
            return this.responseData;
        } catch (error) {
            this.responseData = {
                success: false,
                message: 'Problem Connecting with the Database.',
                error
            };
            return this.responseData;
        }
    }

    async deleteBandMember(bandID) {
        try {
            const headers = await getAuthHeaders();
            const response = await axios.post(deleteBandMemberENDPOINT, { bandID }, { headers });
            this.responseData.success = response.data.success;
            this.responseData.message = response.data.message;
            return this.responseData;
        } catch (error) {
            this.responseData = {
                success: false,
                message: 'Problem Connecting with the Database.',
                error
            };
            return this.responseData;
        }
    }
}

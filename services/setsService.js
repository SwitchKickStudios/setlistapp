import axios from 'axios';
import getAuthHeaders from '../utilities/getAuthHeaders';
import { editSetENDPOINT, deleteSetENDPOINT, createSetENDPOINT } from '../constants/Endpoints';

export default class SetsService {
    constructor() {
        this.returnObject = {};
        this.responseData = {};
    }

    async createSet(setName, setDate, bandID) {
        try {
            const headers = await getAuthHeaders();
            const response = await axios.post(createSetENDPOINT, { setName, setDate, bandID }, { headers });
            this.responseData.success = response.data.success;
            this.responseData.message = response.data.message;
            return this.responseData;
        } catch (error) {
            this.responseData = {
                success: false,
                message: 'Problem Connecting with the Database.',
                error
            };
            return this.responseData;
        }
    }

    async editSet(bandID, setID, newSetName, setDate) {
        try {
            const headers = await getAuthHeaders();
            const response = await axios.post(editSetENDPOINT, {
                bandID,
                setID,
                newSetName,
                setDate
            }, { headers });
            this.responseData.success = response.data.success;
            this.responseData.message = response.data.message;
            return this.responseData;
        } catch (error) {
            this.responseData = {
                success: false,
                message: 'Problem Connecting with the Database.',
                error
            };
            return this.responseData;
        }
    }

    async deleteSet(bandID, setID) {
        try {
            const headers = await getAuthHeaders();
            const response = await axios.post(deleteSetENDPOINT, { bandID, setID }, { headers });
            this.responseData.success = response.data.success;
            this.responseData.message = response.data.message;
            return this.responseData;
        } catch (error) {
            this.responseData = {
                success: false,
                message: 'Problem Connecting with the Database.',
                error
            };
            return this.responseData;
        }
    }
}

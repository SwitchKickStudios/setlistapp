// services/navigator.js
// @flow

import { NavigationActions, StackActions } from 'react-navigation';
import type { NavigationParams, NavigationRoute } from 'react-navigation';

let _container; // eslint-disable-line

function setContainer(container: Object) {
    _container = container;
}

function reset(routeName: string, params?: NavigationParams) {
    _container.navigationRef.dispatch(
        StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({
                    type: 'Navigation/NAVIGATE',
                    routeName,
                    params,
                }),
            ],
        }),
    );
}

function navigate(routeName: string, params?: NavigationParams) {
    _container.navigationRef.dispatch(
        NavigationActions.navigate({
            type: 'Navigation/NAVIGATE',
            routeName,
            params,
        }),
    );
}

function navigateDeep(actions: { routeName: string, params?: NavigationParams }[]) {
    _container.navigationRef.dispatch(
        actions.reduceRight(
            (prevAction, action): any => NavigationActions.navigate({
                type: 'Navigation/NAVIGATE',
                routeName: action.routeName,
                params: action.params,
                action: prevAction,
            }),
            undefined,
        ),
    );
}

function getCurrentRoute(): NavigationRoute | null {
    if (!_container.navigationRef || !_container.navigationRef.state.nav) {
        return null;
    }

    return _container.navigationRef.state.nav.routes[_container.navigationRef.state.nav.index] || null;
}

export default {
    setContainer,
    navigateDeep,
    navigate,
    reset,
    getCurrentRoute,
};

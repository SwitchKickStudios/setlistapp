import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from './redux/app-redux';
import AppSafeAreaView from './AppSafeAreaView';

export default class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                    <AppSafeAreaView />
                </PersistGate>
            </Provider>
        );
    }
}

import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { TextInput } from 'react-native-paper';
import { textInputTheme } from '../themes/theme';

class FormTextInput extends Component {
    render() {
        return (
            <TextInput
                mode={(this.props.mode) ? this.props.mode : 'outlined'}
                label={this.props.label}
                value={this.props.value}
                onChangeText={this.props.onChangeText}
                multiline={this.props.multiline}
                theme={textInputTheme}
                keyboardType={(this.props.keyboardType) ? this.props.keyboardType : 'default'}
                secureTextEntry={this.props.secureTextEntry}
                autoCorrect={false}
                maxLength={(this.props.maxLength) ? this.props.maxLength : 1000}
                autoCapitalize={(this.props.autoCapitalize) ? this.props.autoCapitalize : 'none'}
                style={[styles.input, this.props.style]}
            />
        );
    }
}

const styles = StyleSheet.create({
    input: {
        marginTop: 12
    }
});

export default FormTextInput;

import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';
import { secondaryColor } from '../constants/styleVariables';

class ProfileDrawerButton extends Component {
    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress}>
                <Icon name="user-circle" type="FontAwesome" style={styles.icon} />
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    icon: {
        fontSize: 24,
        color: secondaryColor,
        paddingRight: 12
    },
});

export default ProfileDrawerButton;

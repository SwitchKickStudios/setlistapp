import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import { Icon } from 'native-base';
import { Menu, Divider } from 'react-native-paper';
import { grayTextInputTheme } from '../themes/theme';
import { secondaryColor } from '../constants/styleVariables';
import globalStyles from '../constants/globalStyles';

class ShowHideButton extends Component {
    constructor(props) {
        super(props);

        this.state = {
            menuVisible: false,
        };
    }

    openMenu = () => this.setState({ menuVisible: true });

    closeMenu = () => this.setState({ menuVisible: false });

    renderEyeIcon = (shouldRender) => {
        if (shouldRender) {
            return (
                <Icon name="eye" type="MaterialCommunityIcons" style={styles.eyeIcon} />
            );
        }
        return (
            <Icon name="eye-off" type="MaterialCommunityIcons" style={styles.eyeIconOff} />
        );
    }

    render() {
        return (
            <View>

                <Menu
                    visible={this.state.menuVisible}
                    onDismiss={this.closeMenu}
                    anchor={(
                        <TouchableOpacity style={styles.sortButton} onPress={this.openMenu}>
                            <Icon
                                name="eye-off"
                                type="MaterialCommunityIcons"
                                style={[styles.hideIcon, (this.state.menuVisible) ? globalStyles.miniMenuOpenedAnchor : null]}
                            />
                        </TouchableOpacity>
                    )}
                    style={styles.menu}
                >
                    <View style={globalStyles.miniMenuHeader}>
                        <Text style={globalStyles.extendedHeaderTitle}>Show/Hide</Text>
                    </View>
                    <Divider />
                    <Menu.Item
                        onPress={this.props.onShowDatePress}
                        title="Date"
                        icon={() => this.renderEyeIcon(this.props.showDate)}
                        theme={(!this.props.showDate) ? grayTextInputTheme : null}
                    />
                </Menu>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    menu: {
        marginTop: 33
    },
    hideIcon: {
        fontSize: 24,
        color: secondaryColor,
        right: 8,
    },
    sortButton: {
        flexDirection: 'row',
        alignItems: 'flex-end'
    },
    eyeIcon: {
        fontSize: 24,
        color: secondaryColor,
    },
    eyeIconOff: {
        fontSize: 24,
        color: 'gray',
    },
    sortText: {
        color: secondaryColor,
        fontSize: 14,
        marginBottom: 2
    },
});

export default ShowHideButton;

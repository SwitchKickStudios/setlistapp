import React, { Component } from 'react';
import { FAB } from 'react-native-paper';
import NavigationAwarePortal from './navigationAwarePortal';
import globalStyles from '../constants/globalStyles';

class NavigationAwareFAB extends Component {
    render() {
        return (
            <NavigationAwarePortal>
                <FAB.Group
                    open={this.props.open}
                    onStateChange={this.props.onStateChange}
                    actions={this.props.actions}
                    icon={this.props.icon}
                    visible
                    fabStyle={globalStyles.floatingActionButton}
                />
            </NavigationAwarePortal>
        );
    }
}

export default NavigationAwareFAB;

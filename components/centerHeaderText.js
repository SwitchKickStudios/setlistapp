import React, { Component } from 'react';
import { Text, StyleSheet } from 'react-native';

export default class CenterHeaderText extends Component {
    render() {
        return (
            <Text style={styles.logo}>{this.props.text}</Text>
        );
    }
}

const styles = StyleSheet.create({
    logo: {
        color: 'white',
        fontSize: 18,
    }
});

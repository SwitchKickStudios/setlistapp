import React, { Component } from 'react';
import { View } from 'react-native';
import SimpleItemSeparator from './simpleItemSeparator';
import { headerBorderColor } from '../constants/styleVariables';

class ExtendedHeader extends Component {
    renderDivider() {
        if (!this.props.hideBorder) {
            return <SimpleItemSeparator style={{ backgroundColor: headerBorderColor }} />;
        }
        return null;
    }

    render() {
        return (
            <View>
                <View style={this.props.style}>
                    {this.props.children}
                </View>
                {this.renderDivider()}
            </View>
        );
    }
}

export default ExtendedHeader;

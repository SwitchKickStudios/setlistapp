import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
} from 'react-native';
import Moment from 'moment';
import TableItemDeleteIconButton from './tableItemDeleteIconButton';
import TableItemRenameIconButton from './tableItemRenameIconButton';
import SimpleItemSeparator from './simpleItemSeparator';
import globalStyles from '../constants/globalStyles';

class SetsTableItem extends Component {
    renderDeleteButton() {
        if (this.props.deleteMode) {
            return (
                <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
                    <TableItemDeleteIconButton
                        onDelete={this.props.onDelete}
                        isSelected={this.props.isSelected}
                    />
                </View>
            );
        }
        if (this.props.renameMode) {
            return (
                <TableItemRenameIconButton onRename={this.props.onRename} />
            );
        }
        return null;
    }

    renderDateSection() {
        if (this.props.showDate) {
            const formattedDate = Moment(this.props.date).format('MMMM D, YYYY');
            return (
                <View style={globalStyles.complexCellDateView}>
                    <Text style={[globalStyles.grayLabel, styles.label]}>Date: </Text>
                    <Text style={[globalStyles.musicalKeyText, { fontSize: 13 }]}>{formattedDate}</Text>
                </View>
            );
        }
        return null;
    }

    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress} style={this.props.style} disabled={this.props.deleteMode}>
                <View style={styles.cellRow}>
                    <View style={[
                        globalStyles.complexCellColumn,
                        (this.props.renameMode || this.props.deleteMode) ? globalStyles.complexCellMarginForIcon : null
                    ]}
                    >
                        <View style={styles.itemView}>
                            <Text style={globalStyles.songText}>{this.props.text}</Text>
                        </View>
                        {this.renderDateSection()}
                    </View>
                    {this.renderDeleteButton()}
                </View>
                <SimpleItemSeparator />
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    cellRow: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    itemView: {
        flex: 1,
    },
    label: {
        fontSize: 10,
        paddingBottom: 2
    },
});

export default SetsTableItem;

import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';

class SimpleItemSeparator extends Component {
    render() {
        return (
            <View style={[styles.simpleItemSeparator, this.props.style]} />
        );
    }
}

const styles = StyleSheet.create({
    simpleItemSeparator: {
        backgroundColor: 'lightgray',
        height: 1,
        width: '100%'
    }
});

export default SimpleItemSeparator;

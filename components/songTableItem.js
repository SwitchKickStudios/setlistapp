import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native';
import Moment from 'moment';
import SimpleItemSeparator from './simpleItemSeparator';
import TableItemDeleteIconButton from './tableItemDeleteIconButton';
import globalStyles from '../constants/globalStyles';

class SongTableItem extends Component {
    renderDeleteButton() {
        if (this.props.deleteMode) {
            if (this.props.isLoading) {
                return (
                    <View style={styles.activityIndicatorView}>
                        <ActivityIndicator size="small" color="#000" />
                    </View>
                );
            }
            return (
                <View style={styles.deleteIconView}>
                    <TableItemDeleteIconButton
                        onDelete={this.props.onDelete}
                        deleteButtonText={this.props.deleteButtonText}
                        isSelected={this.props.isSelected}
                        style={styles.deleteIcon}
                    />
                </View>
            );
        }
        return null;
    }

    renderDateSection() {
        if (this.props.showDate) {
            const formattedDate = Moment(this.props.date).format('MMMM D, YYYY');
            return (
                <View style={globalStyles.complexCellDateView}>
                    <Text style={[globalStyles.grayLabel, styles.label]}>Date: </Text>
                    <Text style={[globalStyles.musicalKeyText, { fontSize: 13 }]}>{formattedDate}</Text>
                </View>
            );
        }
        return null;
    }

    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress} style={[styles.container, this.props.style]} disabled={this.props.deleteMode}>
                <View style={styles.itemContainter}>
                    <View style={[globalStyles.complexCellColumn, (this.props.deleteMode) ? globalStyles.complexCellMarginForIcon : null]}>
                        <View style={styles.itemInfoContainer}>
                            {/* LEFT SIDE */}
                            <View style={styles.songAndArtistView}>
                                <Text style={globalStyles.songText}>{this.props.songname}</Text>
                                <Text style={globalStyles.artistText}>{this.props.artist}</Text>
                            </View>
                            {/* RIGHT SIDE */}
                            <View style={styles.detailView}>
                                {/* TEMPO */}
                                <View style={styles.tempoView}>
                                    <Text style={[globalStyles.musicalKeyText]}>{this.props.tempo}</Text>
                                    <Text style={[globalStyles.grayLabel, styles.label]}>{(this.props.tempo) ? 'bpm' : ''}</Text>
                                </View>
                                {/* MUSICAL KEY */}
                                <View style={styles.keyView}>
                                    <Text>
                                        {/* FIRST TWO CHARACTERS */}
                                        <Text style={[globalStyles.musicalKeyText, styles.keyText]}>
                                            {this.props.musicalKey.toString().substring(0, 2)}
                                        </Text>
                                        {/* CONDITIONAL SPACING */}
                                        <Text>{(this.props.musicalKey.toString().length > 2) ? ' ' : null}</Text>
                                        {/* THE REMAINING CHARACTERS */}
                                        <Text style={[globalStyles.grayLabel, styles.label]}>
                                            {this.props.musicalKey.toString().substring(2, this.props.musicalKey.toString().length).trim()}
                                        </Text>
                                    </Text>
                                </View>
                            </View>
                        </View>
                        {this.renderDateSection()}
                    </View>
                    {this.renderDeleteButton()}
                </View>
                <SimpleItemSeparator />
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column'
    },
    itemContainter: {
        flexDirection: 'row',
        width: '100%',
    },
    itemInfoContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    songAndArtistView: {
        flex: 1,
        backgroundColor: 'white'
    },
    detailView: {
        alignItems: 'flex-end',
        width: 100,
        marginLeft: 8
    },
    deleteIconView: {
        justifyContent: 'center',
    },
    tempoView: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        flexShrink: 1
    },
    keyView: {
        paddingTop: 6,
        flexDirection: 'row',
        alignItems: 'flex-end'
    },
    keyText: {
        fontSize: 14,
    },
    label: {
        fontSize: 10,
        paddingBottom: 1
    },
    activityIndicatorView: {
        position: 'absolute',
        right: 0,
        height: '100%',
        width: 40,
        justifyContent: 'center'
    },
});

export default SongTableItem;

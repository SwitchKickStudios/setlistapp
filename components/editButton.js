import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableOpacity
} from 'react-native';
import { Icon } from 'native-base';
import { secondaryColor } from '../constants/styleVariables';

class EditButton extends Component {
    render() {
        return (
            <TouchableOpacity style={[styles.button, this.props.style]} onPress={this.props.onPress}>
                <Icon name="playlist-edit" type="MaterialCommunityIcons" style={styles.icon} />
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        marginRight: 8
    },
    icon: {
        top: 2,
        left: 3,
        fontSize: 32,
        color: secondaryColor,
    },
});

export default EditButton;

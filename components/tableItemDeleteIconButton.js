import React, { Component } from 'react';
import { Icon } from 'native-base';
import { TouchableOpacity, StyleSheet } from 'react-native';

class TableItemDeleteIconButton extends Component {
    render() {
        return (
            <TouchableOpacity style={[styles.deleteButton, this.props.style]} onPress={this.props.onDelete}>
                <Icon
                    name="delete-forever"
                    type="MaterialIcons"
                    style={[styles.deleteButtonIcon, (this.props.isSelected) ? styles.selected : null]}
                />
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    deleteButton: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        right: 12,
        height: 40,
        paddingLeft: 3,
        paddingRight: 3,
        backgroundColor: 'white',
        borderRadius: 5
    },
    deleteButtonIcon: {
        color: 'gray',
        fontSize: 32
    },
    selected: {
        color: 'red',
    }
});


export default TableItemDeleteIconButton;

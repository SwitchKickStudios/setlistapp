import React, { Component } from 'react';
import { Container } from 'native-base';
import { StatusBar } from 'react-native';

class CustomContainer extends Component {
    render() {
        return (
            <Container style={this.props.style} bounces={(this.props.bounces) ? this.props.bounces : true}>
                {this.props.hideStatusBar ? null : (<StatusBar barStyle={(this.props.barStyle) ? this.props.barStyle : 'default'} />)}
                {this.props.children}
            </Container>
        );
    }
}

export default CustomContainer;

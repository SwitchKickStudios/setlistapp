import React, { Component } from 'react';
import {
    TouchableOpacity,
    Text,
    StyleSheet,
    View
} from 'react-native';
import SimpleItemSeparator from './simpleItemSeparator';

class SideBarMenuItem extends Component {
    render() {
        return (
            <View>
                <View style={styles.itemView}>
                    {this.props.icon}
                    <TouchableOpacity
                        onPress={this.props.onPress}
                        style={styles.menuButton}
                    >
                        <Text style={styles.text}>{this.props.text}</Text>
                    </TouchableOpacity>
                </View>
                <SimpleItemSeparator />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    itemView: {
        width: '100%',
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 12
    },
    menuButton: {
        flex: 1,
        justifyContent: 'center',
        marginLeft: 10,
        height: 50,
    },
    text: {
        // fontWeight: '500'
    }
});

export default SideBarMenuItem;

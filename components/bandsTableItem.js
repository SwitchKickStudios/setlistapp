import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';
import TableItemDeleteButton from './tableItemDeleteButton';
import TableItemRenameIconButton from './tableItemRenameIconButton';
import SimpleItemSeparator from './simpleItemSeparator';

class TableItem extends Component {
    renderDeleteButton() {
        if (this.props.deleteMode) {
            return (
                <TableItemDeleteButton
                    onDelete={this.props.onDelete}
                    deleteButtonText={this.props.deleteButtonText}
                    isSelected={this.props.selectedForDelete}
                />
            );
        }
        if (this.props.renameMode) {
            return (
                <TableItemRenameIconButton onRename={this.props.onRename} />
            );
        }
        return null;
    }

    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress} style={this.props.style} disabled={this.props.deleteMode}>
                <View style={styles.cellRow}>
                    <View style={styles.itemView}>{this.props.renderContent}</View>
                    {this.renderDeleteButton()}
                </View>
                <SimpleItemSeparator />
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    cellRow: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    itemView: {
        paddingTop: 16,
        paddingBottom: 16,
        paddingLeft: 16,
        backgroundColor: 'white'
    },
    activityIndicatorView: {
        position: 'absolute',
        right: 0,
        height: '100%',
        width: 40,
        justifyContent: 'center'
    }
});

export default TableItem;

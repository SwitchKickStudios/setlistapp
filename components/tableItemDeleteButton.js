import React, { Component } from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

class TableItemDeleteButton extends Component {
    render() {
        if (this.props.isSelected) {
            return (
                <TouchableOpacity style={styles.deleteButton} onPress={this.props.onDelete}>
                    <Text style={styles.deleteButtonText}>{this.props.deleteButtonText}</Text>
                </TouchableOpacity>
            );
        }
        return (
            <TouchableOpacity style={styles.unselectedButton} onPress={this.props.onDelete}>
                <Text style={styles.unselectedButtonText}>{this.props.deleteButtonText}</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    deleteButton: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        right: 12,
        height: 40,
        width: 100,
        paddingLeft: 8,
        paddingRight: 8,
        backgroundColor: 'red',
        borderRadius: 5
    },
    deleteButtonText: {
        color: 'white'
    },
    unselectedButton: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        right: 12,
        height: 40,
        width: 100,
        paddingLeft: 8,
        paddingRight: 8,
        borderWidth: 2,
        borderRadius: 5,
        borderColor: 'gray'
    },
    unselectedButtonText: {
        color: 'gray'
    },
});


export default TableItemDeleteButton;

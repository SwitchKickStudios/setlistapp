import React, { Component } from 'react';
import { Text, StyleSheet } from 'react-native';
import { Button } from 'native-base';
import { primaryColor } from '../constants/styleVariables';

class WideButton extends Component {
    render() {
        return (
            <Button
                onPress={this.props.onPress}
                style={[styles.wideButton, this.props.buttonStyle, { backgroundColor: this.props.color }]}
                disabled={this.props.disabled}
            >
                <Text
                    style={[
                        styles.buttonText,
                        this.props.textStyle,
                        (this.props.textColor) ? { color: this.props.textColor } : null
                    ]}
                >
                    {this.props.text}
                </Text>
            </Button>
        );
    }
}

const styles = StyleSheet.create({
    wideButton: {
        width: '100%',
        height: 50,
        flex: 0,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 12,
        backgroundColor: primaryColor
    },
    buttonText: {
        color: 'white',
        fontSize: 22
    },
});

export default WideButton;

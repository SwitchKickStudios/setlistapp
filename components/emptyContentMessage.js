import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Content } from 'native-base';

class EmptyContentMessage extends Component {
    render() {
        return (
            <Content contentContainerStyle={{ flexGrow: 1 }} style={{ flexGrow: 1 }} scrollEnabled={false}>
                <View style={styles.messageView}>
                    <Text style={styles.messageText}>{this.props.message}</Text>
                </View>
            </Content>
        );
    }
}

const styles = StyleSheet.create({
    messageView: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '50%'
    },
    messageText: {
        fontSize: 16,
        color: 'gray'
    }
});

export default EmptyContentMessage;

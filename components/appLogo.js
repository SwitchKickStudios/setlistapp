import React, { Component } from 'react';
import { Text, StyleSheet } from 'react-native';
import { secondaryColor } from '../constants/styleVariables';

export default class AppLogo extends Component {
    render() {
        return (
            <Text style={[styles.logo, (this.props.rightPadding) ? styles.rightPadding : null]}>gMetal</Text>
        );
    }
}

const styles = StyleSheet.create({
    logo: {
        color: secondaryColor,
        fontSize: 22,
    },
    rightPadding: {
        paddingRight: 12
    }
});

import React, { Component } from 'react';
import {
    Alert,
    StyleSheet,
    Text,
    TouchableOpacity,
} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import * as firebase from 'firebase';
import {
    SafeAreaView,
    StackActions,
    NavigationActions,
} from 'react-navigation';
import { Content, Icon } from 'native-base';
import { connect } from 'react-redux';
import { setBandsDeleteMode, setCurrentBandID } from '../redux/app-redux';
import { primaryColor } from '../constants/styleVariables';
import SideBarMenuItem from './sideBarMenuItem';
import CustomContainer from './customContainer';

class SideBar extends Component {
    onSignOutPress = () => {
        Alert.alert(
            'Are you sure you want to log out?',
            null,
            [
                {
                    text: 'Cancel',
                    style: 'cancel',
                },
                { text: 'Log Out', onPress: () => this.signOutUser() },
            ],
            { cancelable: false },
        );
    }

    signOutUser = () => {
        this.props.setCurrentBandID(null);
        firebase.auth().signOut();
    }

    onChangeUserNamePress = () => {
        this.props.navigation.navigate('CreateUser');
    }

    onSwitchBandPress = () => {
        this.props.setCurrentBandID(null);
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Bands' })],
        });
        this.props.navigation.dispatch(resetAction);
    }

    // Method for the Leave a Band button
    // Turn DeleteMode on for bands
    // Close drawer
    onLeaveBandPress = () => {
        this.props.setBandsDeleteMode(true);
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Bands' })],
        });
        this.props.navigation.dispatch(resetAction);
        this.props.navigation.closeDrawer();
    }

    getUserDisplayName() {
        const { userDisplayName } = this.props;
        return userDisplayName;
    }

    render() {
        return (
            <SafeAreaView style={styles.safeArea}>
                <CustomContainer
                    style={styles.sideBarView}
                    bounces={false}
                    barStyle="light-content"
                    hideStatusBar={!this.props.navigation.state.isDrawerOpen}
                >
                    {/* User Header */}
                    <LinearGradient style={styles.headerView} colors={['#0F1E25', '#2C3539']}>
                        <Text style={styles.headerText}>{this.getUserDisplayName()}</Text>
                    </LinearGradient>

                    {/* Content */}
                    <Content>
                        <SideBarMenuItem
                            onPress={this.onChangeUserNamePress}
                            text="Change display name"
                            icon={(
                                <Icon
                                    name="account-edit"
                                    type="MaterialCommunityIcons"
                                    style={styles.menuButton}
                                />
                            )}
                        />
                        <SideBarMenuItem
                            onPress={this.onSwitchBandPress}
                            text="Switch Band"
                            icon={(
                                <Icon
                                    name="account-switch"
                                    type="MaterialCommunityIcons"
                                    style={[styles.menuButton, styles.switchBandButton]}
                                />
                            )}
                        />
                        <SideBarMenuItem
                            onPress={this.onSwitchBandPress}
                            text="Start Band"
                            icon={(
                                <Icon
                                    name="guitar-electric"
                                    type="MaterialCommunityIcons"
                                    style={[styles.menuButton, styles.switchBandButton]}
                                />
                            )}
                        />
                        <SideBarMenuItem
                            onPress={this.onSwitchBandPress}
                            text="Join Band"
                            icon={(
                                <Icon
                                    name="group-add"
                                    type="MaterialIcons"
                                    style={[styles.menuButton, styles.switchBandButton]}
                                />
                            )}
                        />
                        <SideBarMenuItem
                            onPress={this.onLeaveBandPress}
                            text="Leave a band"
                            icon={(
                                <Icon
                                    name="account-multiple-minus"
                                    type="MaterialCommunityIcons"
                                    style={[styles.menuButton, styles.leaveBandIcon]}
                                />
                            )}
                        />
                    </Content>

                    {/* Footer */}
                    <TouchableOpacity
                        onPress={this.onSignOutPress}
                        style={styles.signOutButton}
                    >
                        <Text style={styles.buttonText}>Log Out</Text>
                        <Icon
                            name="sign-out"
                            type="FontAwesome"
                            style={styles.signOutButtonIcon}
                        />
                    </TouchableOpacity>
                </CustomContainer>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    sideBarView: {
        flex: 1,
        justifyContent: 'center',
        flexGrow: 1
    },
    safeArea: {
        flex: 1,
        backgroundColor: primaryColor
    },
    headerView: {
        height: 100,
    },
    headerText: {
        color: 'white',
        fontSize: 20,
        paddingLeft: 12,
        position: 'absolute',
        bottom: 11
    },
    menuButton: {
        fontSize: 24,
        color: 'black',
        paddingRight: 12
    },
    switchBandButton: {
        paddingTop: 4
    },
    leaveBandIcon: {
        fontSize: 28,
        paddingRight: 8
    },
    signOutButton: {
        backgroundColor: 'red',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
    },
    signOutButtonIcon: {
        color: 'white',
        position: 'absolute',
        right: 12,
        fontSize: 28
    },
    buttonText: {
        color: 'white',
        fontSize: 22
    },
});

const mapStateToProps = state => ({
    userDisplayName: state.userDisplayName,
    bandsDeleteMode: state.bandsDeleteMode
});

export default connect(mapStateToProps, { setBandsDeleteMode, setCurrentBandID })(SideBar);

import React, { Component } from 'react';
import { Icon } from 'native-base';
import { TouchableOpacity, StyleSheet } from 'react-native';

class TableItemRenameIconButton extends Component {
    render() {
        return (
            <TouchableOpacity style={[styles.renameButton, this.props.style]} onPress={this.props.onRename}>
                <Icon
                    name="pencil"
                    type="MaterialCommunityIcons"
                    style={[styles.renameButtonIcon, (this.props.isSelected) ? styles.selected : null]}
                />
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    renameButton: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        right: 12,
        height: 40,
        paddingLeft: 3,
        paddingRight: 3,
        backgroundColor: 'white',
        borderRadius: 5
    },
    renameButtonIcon: {
        color: 'gray',
        fontSize: 32
    },
});


export default TableItemRenameIconButton;

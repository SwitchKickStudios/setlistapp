import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import SimpleItemSeparator from './simpleItemSeparator';

class SectionHeader extends Component {
    render() {
        return (
            <View style={styles.sectionView}>
                <Text style={styles.sectionText}>{this.props.text}</Text>
                <SimpleItemSeparator />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    sectionView: {
        marginTop: 12
    },
    sectionText: {
        marginBottom: 8,
        marginLeft: 12,
        color: 'darkgray',
        fontSize: 16
    }
});

export default SectionHeader;

import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import { connect } from 'react-redux';
import { Icon } from 'native-base';
import { Menu, Divider } from 'react-native-paper';
import { secondaryColor } from '../constants/styleVariables';
import globalStyles from '../constants/globalStyles';

class SortButton extends Component {
    constructor(props) {
        super(props);

        this.state = {
            menuVisible: false,
        };
    }

    getSetsSortMode() {
        const { setsSortMode } = this.props;
        return setsSortMode;
    }

    openMenu = () => this.setState({ menuVisible: true });

    closeMenu = () => this.setState({ menuVisible: false });

    renderIcon() {
        if (this.getSetsSortMode() === 'nameAsc' || this.getSetsSortMode() === 'dateAsc') {
            return (
                <Icon
                    name="sort-ascending"
                    type="MaterialCommunityIcons"
                    style={[styles.sortIcon, (this.state.menuVisible) ? globalStyles.miniMenuOpenedAnchor : null]}
                />
            );
        }
        return (
            <Icon
                name="sort-descending"
                type="MaterialCommunityIcons"
                style={[styles.sortIcon, (this.state.menuVisible) ? globalStyles.miniMenuOpenedAnchor : null]}
            />
        );
    }

    renderText() {
        if (this.getSetsSortMode() === 'dateAsc') {
            return (
                <Text style={[styles.sortText, styles.sortTextLargeMargin, (this.state.menuVisible) ? globalStyles.miniMenuOpenedAnchor : null]}>
                    date
                </Text>
            );
        }
        if (this.getSetsSortMode() === 'dateDesc') {
            return (
                <Text style={[styles.sortText, (this.state.menuVisible) ? globalStyles.miniMenuOpenedAnchor : null]}>
                    date
                </Text>
            );
        }
        if (this.getSetsSortMode() === 'nameDesc') {
            return (
                <Text style={[styles.sortText, (this.state.menuVisible) ? globalStyles.miniMenuOpenedAnchor : null]}>
                    name
                </Text>
            );
        }
        return (
            <Text style={[styles.sortText, styles.sortTextLargeMargin, (this.state.menuVisible) ? globalStyles.miniMenuOpenedAnchor : null]}>
                name
            </Text>
        );
    }

    render() {
        return (
            <View>

                <Menu
                    visible={this.state.menuVisible}
                    onDismiss={this.closeMenu}
                    anchor={(
                        <TouchableOpacity style={styles.sortButton} onPress={this.openMenu}>
                            {this.renderIcon()}
                            {this.renderText()}
                        </TouchableOpacity>
                    )}
                    style={styles.menu}
                >
                    <View style={globalStyles.miniMenuHeader}>
                        <Text style={globalStyles.extendedHeaderTitle}>Sort</Text>
                    </View>
                    <Divider />
                    <Menu.Item
                        onPress={this.props.onNameAscPress}
                        title="Name Asc"
                        icon={() => (<Icon name="sort-ascending" type="MaterialCommunityIcons" style={styles.sortIcon} />)}
                        style={(this.getSetsSortMode() === 'nameAsc') ? styles.selected : null}
                    />
                    <Menu.Item
                        onPress={this.props.onNameDescPress}
                        title="Name Desc"
                        icon={() => (<Icon name="sort-descending" type="MaterialCommunityIcons" style={styles.sortIcon} />)}
                        style={(this.getSetsSortMode() === 'nameDesc') ? styles.selected : null}
                    />
                    <Divider />
                    <Menu.Item
                        onPress={this.props.onDateAscPress}
                        title="Date Asc"
                        icon={() => (<Icon name="sort-ascending" type="MaterialCommunityIcons" style={styles.sortIcon} />)}
                        style={(this.getSetsSortMode() === 'dateAsc') ? styles.selected : null}
                    />
                    <Menu.Item
                        onPress={this.props.onDateDescPress}
                        title="Date Desc"
                        icon={() => (<Icon name="sort-descending" type="MaterialCommunityIcons" style={styles.sortIcon} />)}
                        style={(this.getSetsSortMode() === 'dateDesc') ? styles.selected : null}
                    />
                </Menu>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    menu: {
        marginTop: 33
    },
    sortButton: {
        flexDirection: 'row',
        alignItems: 'flex-end'
    },
    sortIcon: {
        fontSize: 24,
        color: secondaryColor,
    },
    sortText: {
        color: secondaryColor,
        fontSize: 14,
        marginBottom: 2
    },
    sortTextLargeMargin: {
        marginBottom: 9
    },
    selected: {
        backgroundColor: '#f2f2f2'
    }
});

const mapStateToProps = state => ({
    setsSortMode: state.setsSortMode,
});
export default connect(mapStateToProps, {})(SortButton);

import { Alert } from 'react-native';

export default class CustomAlert {
    static simpleWithCompletion(text, completion) {
        Alert.alert(
            text,
            '',
            [
                {
                    text: 'OK',
                    onPress: completion
                }
            ]
        );
    }
}

import React, { Component } from 'react';
import { Text, StyleSheet } from 'react-native';
import { secondaryColor } from '../constants/styleVariables';

export default class LeftHeaderText extends Component {
    render() {
        return (
            <Text style={styles.logo}>{this.props.text}</Text>
        );
    }
}

const styles = StyleSheet.create({
    logo: {
        color: secondaryColor,
        fontSize: 18,
        paddingLeft: 12
    }
});

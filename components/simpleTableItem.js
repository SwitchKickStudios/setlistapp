import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
} from 'react-native';
import SimpleItemSeparator from './simpleItemSeparator';

class SimpleTableItem extends Component {
    render() {
        return (
            <View style={styles.itemView}>
                <Text style={styles.textStyle}>{this.props.text}</Text>
                <SimpleItemSeparator />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    textStyle: {
        paddingTop: 16,
        paddingBottom: 16,
        paddingLeft: 16,
        backgroundColor: 'white',
        color: 'black',
        fontSize: 18
    },
});

export default SimpleTableItem;

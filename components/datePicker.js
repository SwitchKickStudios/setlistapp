import React, { Component } from 'react';
import {
    StyleSheet,
    View,
} from 'react-native';
import { Button, Icon } from 'native-base';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { TextInput } from 'react-native-paper';
import { datepickerActive, textInputTheme } from '../themes/theme';
import { accentColor1 } from '../constants/styleVariables';

class DatePicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isVisible: false,
            date: new Date(this.props.date)
        };
        this.textInputRef = React.createRef();
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            date: new Date(nextProps.date)
        };
    }

    getRef = () => this.textInputRef;

    showDateTimePicker = () => {
        this.props.onOpenDatePicker();
        this.setState({ isVisible: true, date: new Date(this.props.date) });
    };

    hideDateTimePicker = () => {
        this.setState({ isVisible: false });
    };

    handleDatePicked = (date) => {
        this.props.onConfirm(date);
        this.hideDateTimePicker();
    };

    render() {
        return (
            <View style={styles.datePickerContainer}>
                {/* DISBALES TOUCH EVENTS ON THE TEXT INPUT */}
                <View pointerEvents="none" style={styles.textInputStyle}>
                    <TextInput
                        label="Date"
                        mode="outlined"
                        value={this.props.value}
                        theme={(this.state.isVisible) ? datepickerActive : textInputTheme}
                        multiline={false}
                        editable={false}
                        ref={(input) => { this.textInputRef = input; }}
                    />
                </View>

                <View style={styles.calendarButtonContainer}>
                    <Button onPress={this.showDateTimePicker} style={styles.calendarButton}>
                        <Icon name="calendar" type="MaterialCommunityIcons" style={styles.calendarIcon} />
                    </Button>
                </View>
                <DateTimePicker
                    isVisible={this.state.isVisible}
                    onConfirm={this.handleDatePicked}
                    onCancel={this.hideDateTimePicker}
                    date={this.state.date}
                />
            </View>

        );
    }
}

const styles = StyleSheet.create({
    datePickerContainer: {
        flexDirection: 'row',
        marginTop: 12
    },
    textInputStyle: {
        flex: 1,
    },
    calendarButtonContainer: {
        flexDirection: 'column',
        justifyContent: 'center'
    },
    calendarButton: {
        height: 56,
        width: 56,
        marginLeft: 8,
        marginTop: 8,
        bottom: 1,
        backgroundColor: accentColor1
    },
    calendarIcon: {
        width: '100%',
        textAlign: 'center',
        marginTop: 2,
        marginLeft: 0,
        marginRight: 0,
        fontSize: 28
    }
});

export default DatePicker;

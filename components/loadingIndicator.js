import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { Spinner } from 'native-base';

class LoadingIndicator extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: this.props.isLoading
        };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            isLoading: nextProps.isLoading
        };
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={styles.spinnerView}>
                    <Spinner color="#FFF" />
                </View>
            );
        }
        return null;
    }
}

const styles = StyleSheet.create({
    spinnerTextStyle: {
        color: '#FFF'
    },
    spinnerView: {
        flex: 1,
        justifyContent: 'center',
        width: '100%',
        height: '100%',
        backgroundColor: 'black',
        opacity: 0.4,
        zIndex: 10,
        position: 'absolute'
    }
});

export default LoadingIndicator;

const admin = require('firebase-admin');
const authorizeBandMember = require('./serviceFunctions/authorizeBandMember');

module.exports = async function(req, res) {
    if (!req.body.bandID) {
        return res.status(422).send({ success: false, error: 'Band ID Not Found', message: 'Band ID not found.'});
    }

    // Auth Check - Make sure to pass auth header
    try {
        const auth = await authorizeBandMember(req, res);
        if (!auth.authorized) {
            return res.json({ success: false, message: 'You are not authorized to access this band.' });
        }
    } catch (error) {
        return res.json({ success: false, message: 'Problem authorizing user.' });
    }

    const bandID = req.body.bandID;
    const bandRef = admin.firestore().collection('Bands').doc(bandID);
    const usersArray = [];

    try {
        const bandSnapShot = await bandRef.get();
        if (bandSnapShot.exists) {
            const docData = bandSnapShot.data();
            const userMap = docData.users;
            const userIDArray = [];
            Object.keys(userMap).forEach((user) => {
                userIDArray.push(user);
            });
            const usersArray = await getUserData(userIDArray);
            const usersJSONArrray = JSON.stringify(usersArray);
            return res.json({ success: true, result: `${usersJSONArrray}`, message: 'Succesfull retrieved band data.' });
        }
        return res.json({ success: false, result: `Band not Found - ${bandID}`, message: 'Band not found.' });
    } catch(err) {
        return res.json({ success: false, result: `Failed to obtain band data data for ${bandID}`, error: `${err}`, message: 'Failed to obtain band data data' });
    }
}

async function getUserData(users) {
    const userRef = admin.firestore().collection('Users');
    // const usersArray = [];
    const refArray = [];
    try {
        users.forEach(user => {
            const localRef = admin.firestore().collection('Users').doc(user);
            refArray.push(localRef);
        })

        const userSnapShot = await admin.firestore().getAll(refArray);
        const usersArray = parseUserSnapShot(userSnapShot);

        return usersArray;
    } catch(err) {
        return `Problem querying userbatch - ${err}`;
    }
}

function parseUserSnapShot(querySnapshot) {
    const usersArray = [];
    querySnapshot.forEach( (doc) => {
        const data = doc.data();
        const userObj = {
            name: data.name,
            id: doc.id
        }
        // const name = data.name;
        usersArray.push(userObj);
    });
    return usersArray;
}

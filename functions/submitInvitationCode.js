const admin = require('firebase-admin');
const authorizeUser = require('./serviceFunctions/authorizeUser');


module.exports = async function(req, res) {
    // Validate Request Variables
    if (!req.body.code) {
        return res.status(422).send({ error: 'Invitation Code Not Found' });
    }

    // Auth Check - Make sure to pass auth header
    let uid = '';
    try {
        const auth = await authorizeUser(req, res);
        if (!auth.authorized) {
            return res.json({ success: false, message: "You're session is invalid." });
        }
        uid = auth.decodedToken.uid;
    } catch (error) {
        return res.json({ success: false, message: 'Problem authorizing user.' });
    }

    // Create Local Variables
    const invitationCode = req.body.code;
    let invitationDocument = {};
    let bandDocument = {};
    let deleteResult = {};

    // Query UserID to Validate User
    const userRef = admin.firestore().collection('Users').doc(`${uid}`);
    try {
        const userSnapShot = await userRef.get();
        if (!userSnapShot.exists) {
            return res.json({ success: false, message: 'Invalid User.' });
        }
    } catch (error) {
        return res.json({ success: false, error: error, message: 'Problem Connecting with the Database.' });
    }

    // Query band Invitation Code document
    const inviteRef = admin.firestore().collection('BandInvites').doc(`${invitationCode}`);
    try {
        const invitationSnapshot = await inviteRef.get();
        if (invitationSnapshot.exists) {
            invitationDocument = invitationSnapshot;

            // Delete Invitation Document
            deleteResult = await inviteRef.delete();
        } else {
            return res.json({ success: false, message: 'Unable to Validate Invitation Code.' });
        }
    } catch (error) {
        return res.json({ success: false, error: error, message: 'Problem Connecting with the Database.' });
    }

    // Get bandID from the invitation document
    const bandID = invitationDocument.data().bandID;

    // Query Band Name and validate the band document exists.
    const bandRef = admin.firestore().collection('Bands').doc(`${bandID}`);
    try {
        const bandSnapShot = await bandRef.get();
        if (bandSnapShot.exists) {
            bandDocument = bandSnapShot;
        } else {
            return res.json({ success: false, message: 'Unable to Validate Band ID.' });
        }
    } catch (error) {
        return res.json({ success: false, error: error, message: 'Problem Connecting with the Database.' });
    }

    // Get Band Name from the band document
    const bandname = bandDocument.data().bandData.name;

    // Check if User is already in the Band
    const userExists = bandDocument.data().users[`${uid}`];
    if (userExists) {
        return res.json({ success: true, message: `You are already a member of ${bandname}`, bandname, userExists, deleteResult });
    }

    // Add User to Band
    try {
        await bandRef.update({
            [`users.${uid}`]: {
                write_permission: true
            }
        });
    } catch (error) {
        return res.json({ success: false, error: error, message: 'Problem Adding User to Band.', deleteResult });
    }

    // SUCCESS
    return res.json({ success: true, message: `Successfully Joined ${bandname}`, bandname, deleteResult });
}

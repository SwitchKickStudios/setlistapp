const admin = require('firebase-admin');
const authorizeBandMember = require('./serviceFunctions/authorizeBandMember');
const deleteSongsFromSet = require('./serviceFunctions/deleteSongsFromSet');

module.exports = async function(req, res) {
    if (!req.body.bandID) {
        return res.status(422).send({ error: 'Band ID Not Found', message: 'Band ID Not Found'});
    }
    if (!req.body.setID) {
        return res.status(422).send({ error: 'Set ID Not Found', message: 'Set ID Not Found'});
    }

    // Auth Check - Make sure to pass auth header
    try {
        const auth = await authorizeBandMember(req, res);
        if (!auth.authorized) {
            return res.json({ success: false, message: 'You are not authorized to access this band.' });
        }
    } catch (error) {
        return res.json({ success: false, message: 'Problem authorizing user.' });
    }

    const bandID = req.body.bandID;
    const setID = req.body.setID;
    let setDocument = {};

    // Query the database for Set Document
    const ref = admin.firestore().collection('Bands').doc(`${bandID}`).collection('Sets').doc(`${setID}`);
    try {
        const setSnapShot = await ref.get();
        if (setSnapShot.exists) {
            setDocument = setSnapShot;
        } else {
            return res.json({ success: false, message: 'Invalid Set.' });
        }
    } catch (error) {
        return res.json({ success: false, error: error, message: 'Problem Connecting with the Database.' });
    }

    // Get Set Name from set document
    const setName = setDocument.data().name;

    // Delete Songs from set
    try {
        await deleteSongsFromSet(bandID, setID);
        await ref.delete();
        return res.json({ success: true, message: `Successfully Deleted Set: ${setName}` });
    } catch (error) {
        return res.json({ success: false, error: error, message: 'Problem Connecting with the Database.' });
    }
}

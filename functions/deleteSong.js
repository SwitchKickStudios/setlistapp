const admin = require('firebase-admin');
const authorizeBandMember = require('./serviceFunctions/authorizeBandMember');

module.exports = async function(req, res) {
    if (!req.body.bandID) {
        return res.status(422).send({ error: 'Band ID Not Found'});
    }

    if (!req.body.setID) {
        return res.status(422).send({ error: 'Set ID Not Found'});
    }

    if (!req.body.songID) {
        return res.status(422).send({ error: 'Song ID Not Found'});
    }

    // Auth Check - Make sure to pass auth header
    try {
        const auth = await authorizeBandMember(req, res);
        if (!auth.authorized) {
            return res.json({ success: false, message: 'You are not authorized to access this band.' });
        }
    } catch (error) {
        return res.json({ success: false, message: 'Problem authorizing user.' });
    }


    const bandID = req.body.bandID;
    const setID = req.body.setID;
    const songID = req.body.songID;

    const ref = admin.firestore()
        .collection('Bands').doc(`${bandID}`)
        .collection('Sets').doc(`${setID}`)
        .collection('Songs').doc(`${songID}`);

    ref.get()
        .then((writeResult) => {
            ref.delete();
            return res.json({ result: `Deleted Song: ${songID} from ${setID}` });
        })
        .catch((err) => {
            return res.json({ result: `Failed to obtain song data for ${songID}`, error: `${err}` });
        })

}

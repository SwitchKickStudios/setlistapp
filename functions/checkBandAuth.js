const admin = require('firebase-admin');
const authorizeBandMember = require('./serviceFunctions/authorizeBandMember');

module.exports = async function(req, res) {
    const bandID = req.body.bandID;
    try {
        const auth = await authorizeBandMember(req, res);
        return res.json({
            authorized: auth.authorized,
            error: auth.error,
        });
    } catch (error) {
        return res.json({ authorized: false, error: `Problem authorizing user: ${error}` });
    }
}

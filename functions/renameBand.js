const admin = require('firebase-admin');
const authorizeBandMember = require('./serviceFunctions/authorizeBandMember');
const globalConstants = require('./constants/globalConstants');

module.exports = async function(req, res) {
    // Validate fields
    if (!req.body.newBandName) {
        return res.status(422).send({ error: 'New Band Name Not Found' });
    }
    if (!req.body.bandID) {
        return res.status(422).send({ error: 'Band ID Not Found'});
    }

    // Auth Check - Make sure to pass auth header
    try {
        const auth = await authorizeBandMember(req, res);
        if (!auth.authorized) {
            return res.json({ success: false, message: 'You are not authorized to access this band.' });
        }
    } catch (error) {
        return res.json({ success: false, message: 'Problem authorizing user.' });
    }

    // Create Arguments
    const newBandName = req.body.newBandName.trim();
    const bandID = req.body.bandID;

    // Sanitize Variables
    if (newBandName.length > globalConstants.artistMax) {
        return res.json({ success: false, error: 'Band name is too long', message: 'That band name is too long.' });
    }

    // Query the database for Band Document
    const ref = admin.firestore().collection('Bands').doc(`${bandID}`);
    try {
        const bandUpdateResult = await ref.update({ ['bandData.name']: newBandName });
        if (bandUpdateResult) {
            return res.json({ message: `Successfully renamed band: ${newBandName}`, success: true });
        } else {
            return res.json({ success: false, message: 'Invalid Band.' });
        }
    } catch (error) {
        return res.json({ success: false, error: error, message: 'Problem Connecting with the Database.' });
    }
}

const admin = require('firebase-admin');
const authorizeBandMember = require('./serviceFunctions/authorizeBandMember');
const globalConstants = require('./constants/globalConstants');

module.exports = async function(req, res) {
    // Validate fields
    if (!req.body.bandID) {
        return res.status(422).send({ error: 'Band ID Not Found' });
    }
    if (!req.body.setID) {
        return res.status(422).send({ error: 'Set ID Not Found' });
    }
    // As far as the song parameters go, songName is the only one we validate
    // Artist, tempo, and musical key are optional
    if (!req.body.songName) {
        return res.status(422).send({ error: 'Song Name Not Found' });
    }
    if (req.body.songName.length < 1) {
        return res.status(422).send({ error: 'Invalid Song Name' });
    }

    // Auth Check - Make sure to pass auth header
    try {
        const auth = await authorizeBandMember(req, res);
        if (!auth.authorized) {
            return res.json({ success: false, message: 'You are not authorized to access this band.' });
        }
    } catch (error) {
        return res.json({ success: false, message: 'Problem authorizing user.' });
    }

    // Create Arguments
    const setID = req.body.setID.trim();
    const bandID = req.body.bandID.trim();
    const songName = req.body.songName.trim();
    let artist = "";
    let tempo = "";
    let musicalKey = "";
    const songDate = req.body.songDate;

    // Assign optional parameters if they exist
    if (req.body.artist && req.body.artist.length > 0) {
        artist = req.body.artist.trim();
    }
    if (req.body.tempo && req.body.tempo.length > 0) {
        tempo = req.body.tempo.trim();
    }
    if (req.body.musicalKey && req.body.musicalKey.length > 0) {
        musicalKey = req.body.musicalKey.trim();
    }

    // Sanitize Variables
    if (songName.length > globalConstants.songNameMax) {
        return res.json({ success: false, error: 'Song name is too long', message: `That song name is too long.` });
    }
    if (artist.length > globalConstants.artistMax) {
        return res.json({ success: false, error: 'Artist name is too long', message: `That artist name is too long.` });
    }
    if (tempo.length > globalConstants.tempoMax) {
        return res.json({ success: false, error: 'Tempo is too long', message: `That tempo is too long.` });
    }
    if (musicalKey.length > globalConstants.musicalKeyMax) {
        return res.json({ success: false, error: 'Musical Key is too long', message: `That key is too long.` });
    }
    if (songDate.length > globalConstants.songDateMax) {
        return res.json({ success: false, error: 'Song date is too long', message: 'That date is too long.' });
    }

    // Write to Database
    admin.firestore().collection("Bands").doc(bandID).collection('Sets').doc(setID).collection('Songs').add({
        songName,
        artist,
        tempo,
        musicalKey,
        notes: "",
        date: songDate
    })
        .then((writeResult) => {
            return res.json({ success: true, message: `Successfully created new song: ${songName}` });
        })
        .catch((error) => {
            return res.json({ success: false, error: error, message: `Failed to create new song: ${songName}` });
        });
}

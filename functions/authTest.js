const admin = require('firebase-admin');
// const authorizeBandMember = require('./serviceFunctions/authorizeBandMember');
const authorizeUser = require('./serviceFunctions/authorizeUser');

module.exports = async function(req, res) {
    // const bandID = req.body.bandID;
    try {
        const auth = await authorizeUser(req, res);
        return res.json({
            authorized: auth.authorized,
            error: auth.error,
            decodedToken: auth.decodedToken,
            uid: auth.uid,
            token: auth.token
        });
    } catch (error) {
        return res.json({ authorized: false, error: `Problem authorizing user: ${error}` });
    }
}

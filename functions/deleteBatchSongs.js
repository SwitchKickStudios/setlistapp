const admin = require('firebase-admin');
const authorizeBandMember = require('./serviceFunctions/authorizeBandMember');

module.exports = async function(req, res) {
    if (!req.body.bandID) {
        return res.status(422).send({ error: 'Band ID Not Found'});
    }
    if (!req.body.setID) {
        return res.status(422).send({ error: 'Set ID Not Found'});
    }
    if (!req.body.songIDs) {
        return res.status(422).send({ error: 'Song IDs Not Found'});
    }

    // Auth Check - Make sure to pass auth header
    try {
        const auth = await authorizeBandMember(req, res);
        if (!auth.authorized) {
            return res.json({ success: false, message: 'You are not authorized to access this band.' });
        }
    } catch (error) {
        return res.json({ success: false, message: 'Problem authorizing user.' });
    }

    // Get variables from request
    const bandID = req.body.bandID.trim();
    const setID = req.body.setID.trim();
    const songIDs = req.body.songIDs;

    // Call function to batch delete songs by ID
    const result = await commitSongsForDelete(bandID, setID, songIDs);

    return res.json({
        success: true,
        message: `Successfully Deleted ${result.deletedSongs.length} Songs`,
        failedSongs: result.failedSongs,
        deletedSongs: result.deletedSongs,
        errors: result.errors
    });
}

async function commitSongsForDelete(bandID, setID, songIDs) {
    let deletedSongs = [];
    let failedSongs = [];
    let errors = [];

    var batch = admin.firestore().batch(); // create batch

    // Loop over each song ID, getting and asynchronously deleting each song document
    for (let songID of songIDs) {
        const songRef = admin.firestore()
            .collection('Bands').doc(`${bandID}`)
            .collection('Sets').doc(`${setID}`)
            .collection('Songs').doc(`${songID}`);

        try {
            // Get the song ID, if it exists, delete it
            const songSnapshot = await songRef.get();
            if (songSnapshot.exists) {
                batch.delete(songRef); // add deletion to batch
                deletedSongs.push(songSnapshot.data());
            } else {
                failedSongs.push(songID);
            }
        } catch (error) {
            failedSongs.push(songID);
            errors.push(error);
        }
    }

    batch.commit(); // run batch

    return {deletedSongs, failedSongs, errors };
}

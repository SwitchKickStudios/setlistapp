const admin = require('firebase-admin');
const authorizeBandMember = require('./serviceFunctions/authorizeBandMember');
const deleteEmptyBand_fromDeleteBandMember = require('./serviceFunctions/deleteEmptyBand_fromDeleteBandMember');

module.exports = async function(req, res) {
    // Validate request variables
    if (!req.body.bandID) {
        return res.status(422).send({ error: 'Band ID Not Found', message: 'Band ID Not Found' });
    }

    // Auth Check - Make sure to pass auth header
    let uid = '';
    try {
        const auth = await authorizeBandMember(req, res);
        if (!auth.authorized) {
            return res.json({ success: false, message: 'You are not authorized to access this band.' });
        }
        uid = auth.decodedToken.uid;
    } catch (error) {
        return res.json({ success: false, message: 'Problem authorizing user.' });
    }

    const bandID = req.body.bandID;
    let bandDocument = {};

    var FieldValue = admin.firestore.FieldValue;
    const ref = admin.firestore().collection('Bands').doc(`${bandID}`);

    // Query database for bandDocument
    try {
        const bandSnapshot = await ref.get();
        if (bandSnapshot.exists) {
            bandDocument = bandSnapshot;
        } else {
            return res.json({ success: false, message: 'Invalid Band.' });
        }
    } catch (error) {
        return res.json({ success: false, error: error, message: 'Failed to get band data.' });
    }

    // Get bandData
    const bandData = bandDocument.data();
    const bandName = bandData.bandData.name;
    const users = Object.keys(bandDocument.get('users'));

    // Check if user exists in band
    if (!users.includes(uid)) {
        return res.json({ error: 'User does not exist in band', success: false, users, message: 'User not found in band.' });
    }

    // Remove user from band
    try {
        const updateResult = await ref.update({
            [`users.${uid}`]: FieldValue.delete()
        });

    } catch (error) {
        return res.json({ error: `Failed to remove ${uid} from ${bandID} - ${error}`, success: false, message: 'Problem removing user from band.' });
    }

    if (users.length <= 1) {
        // If this is the final user,
        // delete all sets from the band,
        // then delete the band document
        try {
            const deleteBandResult = await deleteEmptyBand_fromDeleteBandMember(bandID);
            return res.json({ success: true, message: `Successfully Left ${bandName}`, deleteBandResult });
        } catch (error) {
            return res.json({ success: false, error: error, message: 'Problem deleting band.' });
        }
    } else {
        // Otherwise, return Success
        return res.json({ result: `Successfully removed ${uid} from ${bandID}`, success: true, message: `Successfully Left ${bandName}` });
    }
}

const admin = require('firebase-admin');
const uuidv4 = require('uuid/v4');
const twilio = require('./twilio');
const authorizeBandMember = require('./serviceFunctions/authorizeBandMember');
const globalConstants = require('./constants/globalConstants');

module.exports = async function(req, res) {
    if (!req.body.phonenumber) {
        return res.status(422).send({ error: 'Phone Number Not Found' });
    }
    if (!req.body.bandID) {
        return res.status(422).send({ error: 'Band ID Not Found'});
    }

    // Auth Check - Make sure to pass auth header
    try {
        const auth = await authorizeBandMember(req, res);
        if (!auth.authorized) {
            return res.json({ success: false, message: 'You are not authorized to access this band.' });
        }
    } catch (error) {
        return res.json({ success: false, message: 'Problem authorizing user.' });
    }

    const phone = String(req.body.phonenumber).replace(/[^\d]/g, '');
    const bandID = req.body.bandID;
    const uuid = uuidv4();

    // Sanitize Variables
    if (phone.length > globalConstants.phoneNumberMax) {
        return res.json({ success: false, error: 'Phone number is too long', message: `That phone number is too long.` });
    }

    try {
        await twilio.messages.create({
            body: `Step 1: Log into gMetal\n\nStep2: enter the code by following this link:\n\n gMetal://\n exp://10.0.0.54:19000?invitationCode=${uuid}`,
            to: phone,
            from: '+16097957693'
        });
    } catch (error) {
        return res.json({ success: false, message: 'Problem connecting with SMS server', error });
    }

    try {
        await admin.firestore().collection("BandInvites").doc(`${uuid}`).set({
            bandID: bandID
        });
        return res.json({ success: true, message: `An invitation code has been sent to: ${phone}` });
    } catch (error) {
        return res.json({ success: false, message: 'Problem Connecting with the Database.', error });
    }
}

const admin = require('firebase-admin');
const authorizeUser = require('./serviceFunctions/authorizeUser');
const globalConstants = require('./constants/globalConstants');

module.exports = async function(req, res) {
    // Validate fields
    if (!req.body.bandName) {
        return res.status(422).send({ error: 'Band Name Not Found' });
    }

    // Auth Check - Make sure to pass auth header
    let uid = '';
    try {
        const auth = await authorizeUser(req, res);
        if (!auth.authorized) {
            return res.json({ success: false, message: "You're session is invalid." });
        }
        uid = auth.decodedToken.uid;
    } catch (error) {
        return res.json({ success: false, message: 'Problem authorizing user.' });
    }

    // Create Arguments
    const bandName = req.body.bandName.trim();
    const bandData = {
        name: bandName
    };
    const users = {
        [uid]: {
            write_permission: true
        }
    };

    // Sanitize Variables
    if (bandName.length > globalConstants.artistMax) {
        return res.json({ success: false, error: 'Artist name is too long', message: `That artist name is too long.` });
    }

    // Write to Database
    admin.firestore().collection("Bands").add({
        bandData: bandData
        ,users: users
    })
        .then((writeResult) => {
            return res.json({ message: `Successfully created new band: ${bandName}`, success: true });
        })
        .catch((err) => {
            return res.json({ error: `Failed to write new doc - ${bandData} - ${err}`, message: `Failed to create new band: ${bandName}`, success: false });
        });
}

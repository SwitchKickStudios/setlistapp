const admin = require('firebase-admin');

const deleteCollection = (ref, batchSize) => {
    var query = ref.limit(batchSize);

    return new Promise((resolve, reject) => {
        deleteQueryBatch(query, batchSize, resolve, reject);
    });
}

function deleteQueryBatch (query, batchSize, resolve, reject) {
    query.get()
        .then((snapshot) => {
            // When there are no documents left, we are done
            if (snapshot.size === 0) {
                return 0
            }

            // Delete documents in a batch
            var batch = admin.firestore().batch();
            snapshot.docs.forEach((doc) => {
                batch.delete(doc.ref)
            })

            return batch.commit().then(() => {
                return snapshot.size;
            })
        })
        .then((numDeleted) => {
            if (numDeleted <= batchSize) {
                resolve()
                return
            } else {
                // Recurse on the next process tick, to avoid
                // exploding the stack.
                return process.nextTick(() => {
                    deleteQueryBatch(query, batchSize, resolve, reject)
                })
            }
        })
        .catch(reject)
}

module.exports = deleteCollection;

const admin = require('firebase-admin');

const authorizeBandMember = async (req, res) => {
    // GET AUTH TOKEN
    let token = '';
    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
        token = req.headers.authorization.split('Bearer ')[1];
    } else {
        return { authorized: false, error: 'Auth header not found.'};
    }

    // VERIFY AUTH TOKEN
    let decodedToken = {};
    try {
        decodedToken = await admin.auth().verifyIdToken(token);
        const uid = decodedToken.uid;
        return { authorized: true, error: '', decodedToken };
    } catch (error) {
        return { authorized: false, error: `Unable verifying session token: ${error}` };
    }
}

module.exports = authorizeBandMember;

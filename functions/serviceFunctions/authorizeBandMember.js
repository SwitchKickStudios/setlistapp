const admin = require('firebase-admin');

const authorizeBandMember = async (req, res) => {
    // GET AUTH TOKEN
    let token = '';
    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
        token = req.headers.authorization.split('Bearer ')[1];
    } else {
        return { authorized: false, error: 'Auth header not found.'};
    }

    // VERIFY AUTH TOKEN
    let decodedToken = {};
    try {
        decodedToken = await admin.auth().verifyIdToken(token);
    } catch (error) {
        return { authorized: false, error: `Unable verifying session token: ${error}` };
    }

    // VERIFY USER IS IN THE BAND
    const bandID = req.body.bandID.trim();
    const uid = decodedToken.uid;
    const bandRef = admin.firestore().collection("Bands").doc(bandID);
    try {
        const snapShot = await bandRef.get();
        if (snapShot.exists) {
            const bandDoc = snapShot.data();
            if (bandDoc.users[`${uid}`] && bandDoc.users[`${uid}`]['write_permission']) {
                return { authorized: true, error: '', decodedToken };
            }
            return { authorized: false, error: `User not authorized for band: ${uid}`, decodedToken };
        }
        return { authorized: false, error: 'Band not found.'};
    } catch (error) {
        return { authorized: false, error: 'Problem querying band document.'};
    }
}

module.exports = authorizeBandMember;

const admin = require('firebase-admin');
const deleteCollection = require('./deleteCollection');

const deleteSongsFromSet = (bandID, setID) => {
    const songsRef = admin.firestore().collection('Bands').doc(`${bandID}`).collection('Sets').doc(`${setID}`).collection('Songs');
    var promises = [];

    songsRef.get()
        .then((snapshot) => {
            const BATCH_SIZE = 500;
            const deletedSongs = deleteCollection(songsRef, BATCH_SIZE);
            return Promise.all([deletedSongs]);
        })
        .catch((error) => {
            return { success: false, error: `Failed to delete songs - ${error}`};
        });
}

module.exports = deleteSongsFromSet;

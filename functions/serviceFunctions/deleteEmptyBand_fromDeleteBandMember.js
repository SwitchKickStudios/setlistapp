const admin = require('firebase-admin');
const deleteCollection = require('./deleteCollection');
const deleteSongsFromSet = require('./deleteSongsFromSet');

module.exports = async function(bandID, req, res) {
    let bandDocument = {};

    // Query the databse for Band document
    const ref = admin.firestore().collection('Bands').doc(`${bandID}`);
    try {
        const bandSnapshot = await ref.get();
        if (bandSnapshot.exists) {
            bandDocument = bandSnapshot;
        } else {
            return { success: false, message: 'Invalid Band.' };
        }
    } catch (error) {
        return { success: false, error: error, message: 'Failed to get band data.' };
    }

    // Get bandData
    const bandData = bandDocument.data();
    const bandName = bandData.bandData.name;
    const users = Object.keys(bandDocument.get('users'));

    // Check if there is only 1 member in the band.
    if (users.length <= 1) {
        // If the band has one member left,
        // delete all sets from the band,
        // then delete the band document
        try {
            const deleteSetsResult = await deleteSetsFromBand(bandID);
            return { success: true, message: `Successfully deleted band: ${bandName}`, deleteSetsResult };
        } catch (error) {
            return { success: false, error: error, message: 'Problem deleting band.' };
        }
    }
    return { success: true, bandData, message: 'There are still memebers in this band' };
}

// Query collection of Sets
// then prepare the Sets collection for deletion
function deleteSetsFromBand(bandID) {
    const setsRef = admin.firestore().collection('Bands').doc(`${bandID}`).collection('Sets');
    var promises = [];

    setsRef.get()
        .then((snapshot) => {
            const BATCH_SIZE = 500;
            const deletedSets = deleteSetsCollection(setsRef, BATCH_SIZE, bandID);
            return Promise.all([deletedSets]);
        })
        .catch((err) => {
            return { error: `Failed to delete sets - ${err}`};
        });
}

// Limit sets Query for recursive batch delete
function deleteSetsCollection(ref, batchSize, bandID) {
    var query = ref.limit(batchSize);

    return new Promise((resolve, reject) => {
        deleteQuerySetsBatch(query, batchSize, resolve, reject, bandID);
    });
}

// Recursive function that calls limited query snapshots
// then prepares each query of Sets for their Songs to be deleted
function deleteQuerySetsBatch (query, batchSize, resolve, reject, bandID) {
    query.get()
        .then((snapshot) => {
            // When there are no documents left, we are done
            if (snapshot.size === 0) {
                return 0
            }
            prepareBatchSetsForDelete(snapshot, bandID);
            return snapshot.size;
        })
        .then((numDeleted) => {
            if (numDeleted <= batchSize) {
                resolve()
                return
            } else {
                // Recurse on the next process tick, to avoid
                // exploding the stack.
                return process.nextTick(() => {
                    deleteQuerySetsBatch(query, batchSize, resolve, reject, bandID)
                })
            }
        })
        .catch(reject)
}

// Loop over each set in the snapshot
// asynchronously delete the Songs in each Set
async function prepareBatchSetsForDelete(snapshot, bandID) {
    await snapshot.docs.forEach( async (doc) => {
        await prepareSetForDelete(bandID, doc.id)
    });
}

// DELETE SET FUNCTION - Asynchronously:
// Get a single Set documents
// Delete all of the Songs in that document
// Delete the Set document
// When the last set is deleted, delete the Band itself
async function prepareSetForDelete(bandID, setID) {
    let setDocument = {};

    // Query the database for Set Document
    const ref = admin.firestore().collection('Bands').doc(`${bandID}`).collection('Sets').doc(`${setID}`);
    try {
        const setSnapShot = await ref.get();
        if (setSnapShot.exists) {
            setDocument = setSnapShot;
        } else {
            return { success: false, message: 'Invalid Set.' };
        }
    } catch (error) {
        return { success: false, error: error, message: 'Problem Connecting with the Database.' };
    }

    // Get Set Name from set document
    const setName = setDocument.data().name;

    // Delete Songs from set
    try {
        await deleteSongsFromSet(bandID, setID);
        await ref.delete();
        await determineDeleteBand(bandID);
        return { success: true, message: `Successfully Deleted Set: ${setName}` };
    } catch (error) {
        return { success: false, error: error, message: 'Problem Connecting with the Database.' };
    }
}

// DETERMINE IF WE NEED TO DELETE THE BAND
async function determineDeleteBand(bandID) {
    // Query the database for Sets collection
    const setsRef = admin.firestore().collection('Bands').doc(`${bandID}`).collection('Sets');
    let shouldDeleteBand = false;

    try {
        let setsCollectionSnapshot = await setsRef.get();
        if (setsCollectionSnapshot.size === 0) {
            shouldDeleteBand = true;
        }
    } catch (error) {
        return { error: `Failed to obtain set collection - ${error}`};
    }

    if (shouldDeleteBand) {
        // Query the databse for Band document
        const bandRef = admin.firestore().collection('Bands').doc(`${bandID}`);
        let bandDocument = {}
        try {
            const bandSnapshot = await bandRef.get();
            if (bandSnapshot.exists) {
                await bandRef.delete();
                return { success: true, message: 'Successfully deleted Band.' };
            } else {
                return { success: false, message: 'Invalid Band.' };
            }
        } catch (error) {
            return { success: false, error: error, message: 'Failed to get band data.' };
        }
    }

    return { success: true, message: 'Sets still exist in band. We are not deleting Band document.' };

}

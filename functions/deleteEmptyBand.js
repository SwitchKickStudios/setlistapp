const admin = require('firebase-admin');
const authorizeBandMember = require('./serviceFunctions/authorizeBandMember');
const deleteCollection = require('./serviceFunctions/deleteCollection');
const deleteSongsFromSet = require('./serviceFunctions/deleteSongsFromSet');
const deleteEmptyBand_fromDeleteBandMember = require('./serviceFunctions/deleteEmptyBand_fromDeleteBandMember');

module.exports = async function(req, res) {
    if (!req.body.bandID) {
        return res.status(422).send({ error: 'Band ID Not Found'});
    }

    // Auth Check - Make sure to pass auth header
    try {
        const auth = await authorizeBandMember(req, res);
        if (!auth.authorized) {
            return res.json({ success: false, message: 'You are not authorized to access this band.' });
        }
    } catch (error) {
        return res.json({ success: false, message: 'Problem authorizing user.' });
    }

    const bandID = req.body.bandID.trim();

    try {
        const deleteBandResult = await deleteEmptyBand_fromDeleteBandMember(bandID);
        return res.json({ success: true, message: `Successfully deleted band: ${bandName}`, deleteBandResult });
    } catch (error) {
        return res.json({ success: false, error: error, message: 'Problem deleting band.' });
    }
}

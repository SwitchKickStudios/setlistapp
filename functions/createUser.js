const admin = require('firebase-admin');
const authorizeUser = require('./serviceFunctions/authorizeUser');

module.exports = async function(req, res) {
    // Validate fields
    if (!req.body.displayName) {
        return res.status(422).send({ success: false, message: 'Display Name Not Found', error: 'Display Name Not Found' });
    }

    // Auth Check - Make sure to pass auth header
    let uid = '';
    try {
        const auth = await authorizeUser(req, res);
        if (!auth.authorized) {
            return res.json({ success: false, message: "You're session is invalid." });
        }
        uid = auth.decodedToken.uid;
    } catch (error) {
        return res.json({ success: false, message: 'Problem authorizing user.' });
    }

    // Create Arguments
    const displayName = req.body.displayName;

    // Write to Database
    // - Updating or creating the user document based on if it already exists
    let actionResult = {};
    let isNewUser = false;
    try {
        const userExists = await userExistsCheck(uid);
        if (userExists) {
            actionResult = await updateUserDocument(uid, displayName);
        } else {
            isNewUser = true;
            actionResult = await createUserDocument(uid, displayName);
        }
    } catch (error) {
        return res.json({ success: false, message: 'Problem Connecting with the Database.', error });
    }

    if (actionResult.success) {
        if (isNewUser) {
            return res.json({ success: true, message: `Successfully created new user: ${displayName}.`, result: `${actionResult}` });
        }
        return res.json({ success: true, message: `Successfully updated display name: ${displayName}`, result: `${actionResult}` });
    }
    if (isNewUser) {
        return res.json({ success: false, message: `Failed to create new user: ${displayName}.`, result: `${actionResult}` });
    }
    return res.json({ success: false, message: `Failed to update display name: ${displayName}.`, result: `${actionResult}` });
}

async function userExistsCheck(uid) {
    var ref = admin.firestore().collection("Users").doc(`${uid}`);
    try {
        const snapShot = await ref.get();
        if (snapShot.exists) { return true; }
        return false;
    } catch (err) {
        return `Problem querying user - ${err}`;
    }
}

async function createUserDocument(uid, displayName) {
    try {
        await admin.firestore().collection("Users").doc(`${uid}`).set({
            name: displayName,
            secure: {}
        });
        return { success: true, message: `Success creating: ${displayName}` };
    } catch (err) {
        return { success: false, message: `Failed to write new doc - ${displayName} - ${err}` };
    }
}

async function updateUserDocument(uid, displayName) {
    try {
        await admin.firestore().collection("Users").doc(`${uid}`).update({
            name: displayName
        });
        return { success: true, message: `Success updating: ${displayName}` };
    } catch (err) {
        return { success: false, message: `Failed to update doc - ${displayName} - ${err}` };
    }
}

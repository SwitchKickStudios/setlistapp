const admin = require('firebase-admin');
const authorizeBandMember = require('./serviceFunctions/authorizeBandMember');
const globalConstants = require('./constants/globalConstants');

module.exports = async function(req, res) {
    // Validate fields
    if (!req.body.setName) {
        return res.status(422).send({ success: false, message: 'Set Name Not Found', error: 'Set Name Not Found' });
    }
    if (!req.body.setDate) {
        return res.status(422).send({ success: false, message: 'Date Not Found', error: 'Set Date Not Found' });
    }
    if (!req.body.bandID) {
        return res.status(422).send({ success: false, message: 'Band ID Not Found', error: 'Band ID Not Found'});
    }

    // Auth Check - Make sure to pass auth header
    try {
        const auth = await authorizeBandMember(req, res);
        if (!auth.authorized) {
            return res.json({ success: false, message: 'You are not authorized to access this band.' });
        }
    } catch (error) {
        return res.json({ success: false, message: 'Problem authorizing user.' });
    }

    // Create Arguments
    const setName = req.body.setName;
    const setDate = req.body.setDate;
    const bandID = req.body.bandID;

    // Sanitize Variables
    if (setName.length > globalConstants.setMax) {
        return res.json({ success: false, error: 'Set name is too long', message: 'That set name is too long.' });
    }
    if (setDate.length > globalConstants.setDateMax) {
        return res.json({ success: false, error: 'Set date is too long', message: 'That date is too long.' });
    }

    // Write to Database
    admin.firestore().collection("Bands").doc(bandID).collection('Sets').add({
        name: setName,
        date: setDate
    })
        .then((writeResult) => {
            return res.json({ success: true, result: `Success for: ${setName}`, message: 'Successfully Created Set.' });
        })
        .catch((err) => {
            return res.json({ success: false, error: `Failed to write new doc - ${setName} - ${err}`, message: 'Problem Connecting with the Database.' });
        });
}

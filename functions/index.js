// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// --- Importing functions ---
// const functionName = require('./function_file');
// exports.functionName = functions.https.onRequest(functionName);

const admin = require('firebase-admin');
const functions = require('firebase-functions');
const serviceAccount = require('./service_account.json');

const createUser = require('./createUser');
const createBand = require('./createBand');
const renameBand = require('./renameBand');
const createSet = require('./createSet');
const createSong = require('./createSong');
const editSet = require('./editSet');
const inviteBandMember = require('./inviteBandMember');
const submitInvitationCode = require('./submitInvitationCode');
const deleteBandMember = require('./deleteBandMember');
const deleteEmptyBand = require('./deleteEmptyBand');
const deleteSet = require('./deleteSet');
const deleteSong = require('./deleteSong');
const deleteBatchSongs = require('./deleteBatchSongs');
const getBandDetails = require('./getBandDetails');
const checkBandAuth = require('./checkBandAuth');

const authTest = require('./authTest');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://setlistapp-2cc45.firebaseio.com"
});

exports.createUser = functions.https.onRequest(createUser);
exports.createBand = functions.https.onRequest(createBand);
exports.renameBand = functions.https.onRequest(renameBand);
exports.createSet = functions.https.onRequest(createSet);
exports.createSong = functions.https.onRequest(createSong);
exports.editSet = functions.https.onRequest(editSet);
exports.inviteBandMember = functions.https.onRequest(inviteBandMember);
exports.submitInvitationCode = functions.https.onRequest(submitInvitationCode);
exports.deleteBandMember = functions.https.onRequest(deleteBandMember);
exports.deleteEmptyBand = functions.https.onRequest(deleteEmptyBand);
exports.deleteSet = functions.https.onRequest(deleteSet);
exports.deleteSong = functions.https.onRequest(deleteSong);
exports.deleteBatchSongs = functions.https.onRequest(deleteBatchSongs);
exports.getBandDetails = functions.https.onRequest(getBandDetails);
exports.checkBandAuth = functions.https.onRequest(checkBandAuth);

exports.authTest = functions.https.onRequest(authTest);

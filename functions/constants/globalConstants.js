// MUST MATCH WITH APP GLOBAL CONSTANTS
const globalConstants = {
    songNameMax: 80,
    artistMax: 60,
    musicalKeyMax: 40,
    tempoMax: 3,
    songNotesMax: 3000,
    phoneNumberMax: 25,
    displayNameMax: 50,
    setMax: 140,
    setDateMax: 50
};

module.exports = globalConstants;

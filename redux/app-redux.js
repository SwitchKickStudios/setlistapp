import { createStore, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import createSecureStore from 'redux-persist-expo-securestore';
import thunkMiddleware from 'redux-thunk';
import 'firebase/firestore';

// INITIAL STATE
const initialState = {
    isLoading: false,
    rememberUsername: false,
    storedUsername: null,
    currentBandID: null,
    userDisplayName: null,
    bandsDeleteMode: false,
    bandsRenameMode: false,
    setsShowDate: true,
    setsSortMode: 'nameAsc',
    songsShowDate: true,
    songsSortMode: 'nameAsc',
};

// REDUCER
const reducer = (state = initialState, action) => {
    switch (action.type) {
    case 'INITIALIZE_STATE':
        return state;
    case 'SET_IS_LOADING':
        return { ...state, isLoading: action.value };
    case 'SET_REMEMBER_USERNAME':
        return { ...state, rememberUsername: action.value };
    case 'SET_STORED_USERNAME':
        return { ...state, storedUsername: action.value };
    case 'SET_CURRENT_BANDID':
        return { ...state, currentBandID: action.value };
    case 'SET_USER_DISPLAYNAME':
        return { ...state, userDisplayName: action.value };
    case 'SET_BANDS_DELETEMODE':
        return { ...state, bandsDeleteMode: action.value };
    case 'SET_BANDS_RENAMEMODE':
        return { ...state, bandsRenameMode: action.value };
    case 'SET_SETS_SHOWDATE':
        return { ...state, setsShowDate: action.value };
    case 'SET_SETS_SORTMODE':
        return { ...state, setsSortMode: action.value };
    case 'SET_SONGS_SHOWDATE':
        return { ...state, songsShowDate: action.value };
    case 'SET_SONGS_SORTMODE':
        return { ...state, songsSortMode: action.value };
    default:
        break;
    }
    return state;
};

// STORE
const storage = createSecureStore();
const persistConfig = {
    key: 'root',
    storage,
};

const persistedReducer = persistReducer(persistConfig, reducer);
const store = createStore(persistedReducer, applyMiddleware(thunkMiddleware));
const persistor = persistStore(store);

export { store, persistor };


// ACTION CREATORS
const initializeState = () => ({
    type: 'INITIALIZE_STATE'
});
const setIsLoading = isLoading => ({
    type: 'SET_IS_LOADING',
    value: isLoading
});
const setRememberUsername = rememberUsername => ({
    type: 'SET_REMEMBER_USERNAME',
    value: rememberUsername
});
const setStoredUsername = username => ({
    type: 'SET_STORED_USERNAME',
    value: username
});
const setCurrentBandID = bandID => ({
    type: 'SET_CURRENT_BANDID',
    value: bandID
});
const setUserDisplayName = displayName => ({
    type: 'SET_USER_DISPLAYNAME',
    value: displayName
});
const setBandsDeleteMode = deleteMode => ({
    type: 'SET_BANDS_DELETEMODE',
    value: deleteMode
});
const setBandsRenameMode = renameMode => ({
    type: 'SET_BANDS_RENAMEMODE',
    value: renameMode
});
const setSetsShowDate = showDate => ({
    type: 'SET_SETS_SHOWDATE',
    value: showDate
});
const setSetsSortMode = sortMode => ({
    type: 'SET_SETS_SORTMODE',
    value: sortMode
});
const setSongsShowDate = showDate => ({
    type: 'SET_SONGS_SHOWDATE',
    value: showDate
});
const setSongsSortMode = sortMode => ({
    type: 'SET_SONGS_SORTMODE',
    value: sortMode
});

export {
    initializeState,
    setIsLoading,
    setRememberUsername,
    setStoredUsername,
    setCurrentBandID,
    setUserDisplayName,
    setBandsDeleteMode,
    setBandsRenameMode,
    setSetsShowDate,
    setSetsSortMode,
    setSongsShowDate,
    setSongsSortMode
};

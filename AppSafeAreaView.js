import React from 'react';
import { Linking } from 'expo';
import { StyleSheet, View } from 'react-native';
import { Provider } from 'react-native-paper';
import { SafeAreaView } from 'react-navigation';
import * as firebase from 'firebase';
import { connect } from 'react-redux';
import { setIsLoading } from './redux/app-redux';
import RootNavigation from './navigation/RootNavigation';
import MainDrawerNavigation from './navigation/MainDrawerNavigation';
import SplashScreen from './screens/SplashScreen';
import ApiKeys from './constants/ApiKeys';
import 'firebase/firestore';
import LoadingIndicator from './components/loadingIndicator';
import NavigatorService from './services/navigatorService';

class AppSafeAreaView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isAuthenticationReady: false,
            isAuthenticated: false,
        };

        // Initialize Firebase...
        if (!firebase.apps.length) { firebase.initializeApp(ApiKeys.FirebaseConfig); }
        const firestore = firebase.firestore();
        firestore.settings({});
        firebase.auth().onAuthStateChanged(this.onAuthStateChanged);
    }

    componentDidMount() {
        this.addLinkingListener();
    }

    onAuthStateChanged = (user) => {
        this.setState({ isAuthenticationReady: true });
        this.setState({ isAuthenticated: !!user });
    }

    getIsLoading() {
        const { isLoading } = this.props;
        return isLoading;
    }

    handleRedirect = (event) => {
        const data = Linking.parse(event.url);
        const invitationCode = data.queryParams.invitationCode;
        NavigatorService.navigate('JoinBand', {
            invitationCode,
        });
    };

    addLinkingListener = () => {
        Linking.addEventListener('url', this.handleRedirect);
    };

    _removeLinkingListener = () => {
        Linking.removeEventListener('url', this.handleRedirect);
    };


    render() {
        if (!this.state.isAuthenticationReady) {
            return (
                <SafeAreaView style={styles.safeArea} forceInset={{ top: 'never' }}>
                    <SplashScreen />
                </SafeAreaView>
            );
        }
        return (
            <SafeAreaView style={styles.safeArea} forceInset={{ top: 'never' }}>
                <React.Fragment>
                    <Provider>
                        <LoadingIndicator isLoading={this.getIsLoading()} />
                        <View style={styles.container}>
                            {
                                (this.state.isAuthenticated)
                                    ? <MainDrawerNavigation ref={(navigatorRef) => { NavigatorService.setContainer(navigatorRef); }} />
                                    : <RootNavigation />
                            }
                        </View>
                    </Provider>
                </React.Fragment>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    safeArea: {
        flex: 1,
        backgroundColor: 'white'
    }
});

const mapStateToProps = state => ({ isLoading: state.isLoading });
export default connect(mapStateToProps, { setIsLoading })(AppSafeAreaView);
